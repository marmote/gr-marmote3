/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "keep_large_segments_impl.h"
#include <gnuradio/io_signature.h>
#include <marmote3/marmote3.pb.h>

namespace gr {
namespace marmote3 {

keep_large_segments::sptr keep_large_segments::make(int itemsize, bool enabled,
                                                    long skip, long keep,
                                                    long period, long total,
                                                    int report_id) {
  return gnuradio::get_initial_sptr(new keep_large_segments_impl(
      itemsize, enabled, skip, keep, period, total, report_id));
}

keep_large_segments_impl::keep_large_segments_impl(int itemsize, bool enabled,
                                                   long skip, long keep,
                                                   long period, long total,
                                                   int report_id)
    : gr::block("keep_large_segments", gr::io_signature::make(1, 1, itemsize),
                gr::io_signature::make(1, 1, itemsize)),
      itemsize(itemsize), skip(skip), keep(keep), period(period), total(total),
      report_id(report_id), enabled_consumed(0), total_consumed(0),
      total_produced(0), atomic_enabled(enabled) {
  if (itemsize <= 0)
    throw std::out_of_range("keep_large_segments: invalid item size");

  if (skip < 0)
    throw std::out_of_range("keep_large_segments: invalid skip value");

  if (keep < 0)
    throw std::out_of_range("keep_large_segments: invalid keep value");

  if (period <= 0 || keep > period)
    throw std::out_of_range("keep_large_segments: invalid period value");

  if (total < -1)
    throw std::out_of_range("keep_large_segments: invalid total value");
}

keep_large_segments_impl::~keep_large_segments_impl() {}

void keep_large_segments_impl::execute(const unsigned char *input,
                                       long input_len, unsigned char *output,
                                       long output_len, long &consumed,
                                       long &produced) {
  long enabled_consumed2 = enabled_consumed;
  long total_produced2 = total_produced;

  long position =
      enabled_consumed2 > skip ? (enabled_consumed2 - skip) % period : 0;
  consumed = 0;
  produced = 0;

  if (false)
    std::cout << (input == NULL ? "forecast" : "work") << std::endl;

  while (consumed < input_len && produced < output_len) {
    if (enabled_consumed2 + consumed < skip) {
      consumed +=
          std::min(input_len - consumed, skip - (enabled_consumed2 + consumed));
    } else if (total >= 0 && total_produced2 + produced >= total) {
      consumed = input_len;
    } else if (input == NULL && input_len - consumed >= period) {
      long amount = (input_len - consumed) / period;
      consumed += amount * period;
      produced += amount * keep;
      if (total >= 0)
        produced = std::min(total, produced);
    } else if (position >= keep) {
      long amount = std::min(input_len - consumed, period - position);
      consumed += amount;
      position += amount;
      if (position >= period)
        position = 0;
    } else {
      long amount = std::min(keep - position, output_len - produced);
      if (total >= 0)
        amount = std::min(amount, total - (total_produced2 + produced));
      amount = std::min(amount, input_len - consumed);

      if (input != NULL)
        std::memcpy(output + produced * itemsize, input + consumed * itemsize,
                    amount * itemsize);

      consumed += amount;
      produced += amount;
      position += amount;
      if (position >= period)
        position = 0;
    }

    if (false)
      std::cout << enabled_consumed2 << " " << total_produced2 << " "
                << input_len << " " << output_len << " " << consumed << " "
                << produced << std::endl;
  }
}

void keep_large_segments_impl::forecast(int noutput_items,
                                        gr_vector_int &ninput_items_required) {
  long consumed, produced;
  execute(NULL, noutput_items, NULL, noutput_items, consumed, produced);
  ninput_items_required[0] = consumed;
}

int keep_large_segments_impl::general_work(
    int noutput_items, gr_vector_int &ninput_items,
    gr_vector_const_void_star &input_items, gr_vector_void_star &output_items) {
  if (atomic_enabled) {
    const unsigned char *in = (const unsigned char *)input_items[0];
    unsigned char *out = (unsigned char *)output_items[0];

    long consumed, produced;
    execute(in, ninput_items[0], out, noutput_items, consumed, produced);

    enabled_consumed += consumed;
    total_consumed += consumed;
    total_produced += produced;

    consume(0, consumed);
    return produced;
  } else {
    consume(0, ninput_items[0]);
    return 0;
  }
}

void keep_large_segments_impl::set_enabled(bool value) {
  atomic_enabled = value;
}

void keep_large_segments_impl::collect_block_report(ModemReport *modem_report) {
  if (report_id >= 0) {
    KeepSegmentsReport *report = modem_report->add_keep_segments();

    report->set_report_id(report_id);
    report->set_consumed(total_consumed);
    report->set_produced(total_produced);
  }
}

} /* namespace marmote3 */
} /* namespace gr */
