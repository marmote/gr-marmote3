/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "vector_fir_filter_fff_impl.h"
#include <gnuradio/io_signature.h>

namespace gr {
namespace marmote3 {

vector_fir_filter_fff::sptr
vector_fir_filter_fff::make(int vlen, const std::vector<float> &taps) {
  return gnuradio::get_initial_sptr(new vector_fir_filter_fff_impl(vlen, taps));
}

vector_fir_filter_fff_impl::vector_fir_filter_fff_impl(
    int vlen, const std::vector<float> &taps)
    : gr::sync_block("vector_fir_filter_fff",
                     gr::io_signature::make(1, 1, sizeof(float) * vlen),
                     gr::io_signature::make(1, 1, sizeof(float) * vlen)),
      vlen(vlen), taps(taps) {
  if (vlen <= 0 || vlen % 4 != 0)
    throw std::invalid_argument(
        "vector_fir_filter_fff: vector length must be divisible by 4");

  if (taps.size() <= 0)
    throw std::invalid_argument("vector_fir_filter_fff: empty taps list");

  set_history(taps.size());
}

vector_fir_filter_fff_impl::~vector_fir_filter_fff_impl() {}

int vector_fir_filter_fff_impl::work(int noutput_items,
                                     gr_vector_const_void_star &input_items,
                                     gr_vector_void_star &output_items) {
  const float *in = (const float *)input_items[0];
  float *out = (float *)output_items[0];

  for (int n = 0; n < noutput_items; n++) {
    work_cpu_a(in, out);
    in += vlen;
    out += vlen;
  }

  return noutput_items;
}

void vector_fir_filter_fff_impl::work_cpu_a(const float *in, float *out) {
  float c = taps[0];
  for (int j = 0; j < vlen; j += 4) {
    out[j + 0] = c * in[j + 0];
    out[j + 1] = c * in[j + 1];
    out[j + 2] = c * in[j + 2];
    out[j + 3] = c * in[j + 3];
  }
  in += vlen;

  for (int i = 1; i < taps.size(); i++) {
    c = taps[i];
    for (int j = 0; j < vlen; j += 4) {
      out[j + 0] += c * in[j + 0];
      out[j + 1] += c * in[j + 1];
      out[j + 2] += c * in[j + 2];
      out[j + 3] += c * in[j + 3];
    }
    in += vlen;
  }
}

void vector_fir_filter_fff_impl::work_cpu_b(const float *in, float *out) {
  for (int i = 0; i < vlen; i += 4) {
    const float *in2 = in;
    float c[4];

    float t = taps[0];
    c[0] = t * in2[0];
    c[1] = t * in2[1];
    c[2] = t * in2[2];
    c[3] = t * in2[3];
    in2 += vlen;

    for (int j = 1; j < taps.size(); j++) {
      t = taps[j];
      c[0] += t * in2[0];
      c[1] += t * in2[1];
      c[2] += t * in2[2];
      c[3] += t * in2[3];
      in2 += vlen;
    }

    out[0] = c[0];
    out[1] = c[1];
    out[2] = c[2];
    out[3] = c[3];

    out += 4;
    in += 4;
  }
}

} /* namespace marmote3 */
} /* namespace gr */
