/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_MODEM_RECEIVER_IMPL_H
#define INCLUDED_MARMOTE3_MODEM_RECEIVER_IMPL_H

#include <unordered_map>

#include <marmote3/headers.h>
#include <marmote3/modem_receiver.h>

namespace gr {
namespace marmote3 {

class modem_receiver_impl : public modem_receiver {
private:
  const int payload_mode;
  const pmt::pmt_t port_id;
  const modem_sender2::sptr modem_sender;
  const bool debug_scheduler;

public:
  modem_receiver_impl(int payload_mode, modem_sender2::sptr modem_sender,
                      bool debug_scheduler);
  ~modem_receiver_impl();

  int work(int noutput_items, gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items) override;

  void collect_block_report(ModemReport *modem_report) override;

private:
  struct flow_key_t {
    int source = -1;
    int destination = -1;
    int flowid = -1;
    int channel = -1;
    int mcs = -1;
    bool crc = false;

    inline bool operator==(const flow_key_t &key) const {
      return source == key.source && destination == key.destination &&
             flowid == key.flowid && channel == key.channel && mcs == key.mcs &&
             crc == key.crc;
    }
  };

  struct flow_hash_t {
    inline size_t operator()(const flow_key_t &key) const {
      return (key.source << 24) + (key.flowid << 16) + (key.destination << 8) +
             key.channel + (key.mcs << 4) + (key.crc ? 128 : 0);
    }
  };

  struct flow_val_t {
    int packets_received = 0; // number of received messages
    long bytes_received = 0;  // total number of received bytes
    int packets_missing = 0;  // we did not receive some messages
    double power = 0.0;       // total equalizer reported power
    double snr = 0.0;         // total equalizer reported snr
    double freq_err1 = 0.0;   // total frequency error
    double phase_err = 0.0;   // total phase error
    double freq_err0 = 0.0;   // total frequency error in preamble
  };

  struct rate_key_t {
    int mcs;
    int snr;

    inline bool operator==(const rate_key_t &key) const {
      return mcs == key.mcs && snr == key.snr;
    }
  };

  struct rate_hash_t {
    inline size_t operator()(const rate_key_t &key) const {
      return (key.mcs << 8) + key.snr;
    }
  };

  struct rate_val_t {
    unsigned int passed = 0;
    unsigned int failed = 0;
  };

  struct flow_seqno_key_t {
    int source = -1;
    int destination = -1;
    int flowid = -1;

    inline bool operator==(const flow_seqno_key_t &key) const {
      return source == key.source && destination == key.destination &&
             flowid == key.flowid;
    }
  };

  struct flow_seqno_hash_t {
    inline size_t operator()(const flow_seqno_key_t &key) const {
      return (key.source << 24) + (key.flowid << 8) + key.destination;
    }
  };

  struct flow_seqno_val_t {
    int last_seqno = arq_header_t::initial_seqno;
    int age = 0; // incremented with report
  };

  std::unordered_map<flow_key_t, flow_val_t, flow_hash_t> flows;
  std::unordered_map<rate_key_t, rate_val_t, rate_hash_t> rates;
  std::unordered_map<flow_seqno_key_t, flow_seqno_val_t, flow_seqno_hash_t>
      flow_seqnos;
  gr::thread::mutex mutex;
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_MODEM_RECEIVER_IMPL_H */
