/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_PACKET_FREQ_CORRECTOR_IMPL_H
#define INCLUDED_MARMOTE3_PACKET_FREQ_CORRECTOR_IMPL_H

#include <marmote3/packet_freq_corrector.h>

namespace gr {
namespace marmote3 {

class packet_freq_corrector_impl : public packet_freq_corrector {
private:
  const modulation_t mod;
  const int span;
  const bool postamble;
  bool small_input_warning;

protected:
  void method_bpsk(const gr_complex *in, int dir, int len, float &phase_err,
                   float &freq_err);
  void method_qpsk(const gr_complex *in, int dir, int len, float &phase_err,
                   float &freq_err);
  void method_8psk(const gr_complex *in, int dir, int len, float &phase_err,
                   float &freq_err);

public:
  packet_freq_corrector_impl(modulation_t mod, int span, bool postamble,
                             int max_output_len);
  ~packet_freq_corrector_impl();

  int work(int noutput_items, gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_PACKET_FREQ_CORRECTOR_IMPL_H */
