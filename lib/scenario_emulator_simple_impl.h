/* -*- c++ -*- */
/*
 * Copyright 2017 Peter Horvath.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_SCENARIO_EMULATOR_SIMPLE_IMPL_H
#define INCLUDED_MARMOTE3_SCENARIO_EMULATOR_SIMPLE_IMPL_H

#include "fading_generator.h"
#include <marmote3/scenario_emulator_simple.h>

namespace gr {
namespace marmote3 {

class scenario_emulator_simple_impl : public scenario_emulator_simple {
private:
  float samp_rate;
  float fdopp_abs;
  float update_interval;
  float K_fact;
  int no_sines;

  const float norm_doppler;
  const int update_samples;
  int update_cnt;
  fading_generator_correlated *fg;

public:
  scenario_emulator_simple_impl(float fs, float fdopp, float update_interval,
                                float K_fact, int no_sines);
  ~scenario_emulator_simple_impl();

  // Where all the action really happens
  int work(int noutput_items, gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_SCENARIO_EMULATOR_SIMPLE_IMPL_H */
