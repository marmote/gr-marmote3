/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "packet_to_message_impl.h"
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <sstream>

namespace gr {
namespace marmote3 {

packet_to_message::sptr packet_to_message::make(int itemsize) {
  return gnuradio::get_initial_sptr(new packet_to_message_impl(itemsize));
}

packet_to_message_impl::packet_to_message_impl(int itemsize)
    : tagged_stream_block2("packet_to_message",
                           gr::io_signature::make(1, 1, itemsize),
                           gr::io_signature::make(0, 0, 0), 0),
      itemsize(itemsize), port_id(pmt::mp("out")) {
  if (itemsize != 1 && itemsize != 4 && itemsize != 8)
    throw std::out_of_range("packet_to_message: incorrect item size");

  message_port_register_out(port_id);
}

packet_to_message_impl::~packet_to_message_impl() {}

int packet_to_message_impl::work(int noutput_items, gr_vector_int &ninput_items,
                                 gr_vector_const_void_star &input_items,
                                 gr_vector_void_star &output_items) {
  const unsigned char *in = (const unsigned char *)input_items[0];

  pmt::pmt_t payload;
  if (itemsize == 1)
    payload = pmt::init_u8vector(ninput_items[0], in);
  else if (itemsize == 4)
    payload = pmt::init_f32vector(ninput_items[0], (const float *)in);
  else if (itemsize == 8)
    payload = pmt::init_c32vector(ninput_items[0], (const gr_complex *)in);
  else
    payload = PMT_NULL; // this should not happen

  pmt::pmt_t msg = pmt::cons(output_dict, payload);
  message_port_pub(port_id, msg);

  if (false) {
    std::stringstream msg;
    msg << "packet_to_message: moved " << ninput_items[0] << " items packet\n";
    std::cout << msg.str();
  }

  return 0;
}

} /* namespace marmote3 */
} /* namespace gr */
