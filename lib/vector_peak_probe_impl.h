/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_VECTOR_PEAK_PROBE_IMPL_H
#define INCLUDED_MARMOTE3_VECTOR_PEAK_PROBE_IMPL_H

#include <gnuradio/thread/thread.h>
#include <marmote3/vector_peak_probe.h>
#include <vector>

namespace gr {
namespace marmote3 {

class vector_peak_probe_impl : public vector_peak_probe {
private:
  const int vlen;
  const int report_type;
  const int histogram_bins;
  const float histogram_low_db;
  const float histogram_step_db;

  struct carrier_t {
    double energy = 0;
    float maximum = 0;
    unsigned int last_bin = 0;
    std::vector<unsigned int> histogram;
  };

  std::vector<carrier_t> carriers;
  unsigned long samples;
  gr::thread::mutex mutex;

  std::vector<float> temp;
  std::vector<unsigned int> temp_bins;
  int stalled_count;

public:
  vector_peak_probe_impl(int vlen, int report_type, int histogram_bins,
                         float histogram_low_db, float histogram_step_db);
  ~vector_peak_probe_impl();

  int work(int noutput_items, gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);

  void collect_block_report(ModemReport *modem_report) override;

  std::vector<float> get_maximum_db() override;
  std::vector<unsigned int> get_power_bins() override;
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_VECTOR_PEAK_PROBE_IMPL_H */
