/* -*- c++ -*- */
/*
 * Copyright 2014-2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "polyphase_synt_filter_ccf_impl.h"
#include "xmmintrin.h"
#include <cstring>
#include <gnuradio/io_signature.h>

namespace gr {
namespace marmote3 {

polyphase_synt_filter_ccf::sptr
polyphase_synt_filter_ccf::make(int vlen, const std::vector<float> &taps,
                                int stride) {
  return gnuradio::get_initial_sptr(
      new polyphase_synt_filter_ccf_impl(vlen, taps, stride));
}

polyphase_synt_filter_ccf_impl::polyphase_synt_filter_ccf_impl(
    int vlen, const std::vector<float> &taps_arg, int stride)
    : gr::block("polyphase_synt_filter_ccf",
                gr::io_signature::make(1, 1, sizeof(gr_complex) * vlen),
                gr::io_signature::make(1, 1, sizeof(gr_complex))),
      vlen(vlen), stride(stride), taps_len(taps_arg.size()),
      work_len((taps_arg.size() + vlen - 1) / vlen * vlen + 3) {
  if (stride <= 0)
    throw std::invalid_argument(
        "polyphase_synt_filter_ccf: stride must be positive");

  if (vlen <= 0)
    throw std::invalid_argument(
        "polyphase_synt_filter_ccf: vector length pust be positive");

  if (vlen % 4 != 0 && vlen != 25 && vlen != 30 && vlen != 50)
    std::cout << alias() << ": WARNING non-optimal vector length " << vlen
              << std::endl;

  if (work_len < stride)
    throw std::invalid_argument(
        "polyphase_synt_filter_ccf: invalid taps length");

  if (posix_memalign((void **)&taps, 16, work_len * sizeof(float)) != 0)
    throw std::bad_alloc();

  for (int i = 0; i < work_len; ++i)
    taps[i] = i < taps_arg.size() ? taps_arg[i] : 0.0f;

  set_min_noutput_items(work_len);
  set_min_output_buffer(0, 2 * work_len);
  set_relative_rate(stride);
}

polyphase_synt_filter_ccf_impl::~polyphase_synt_filter_ccf_impl() {
  free(taps);
  taps = NULL;
}

void polyphase_synt_filter_ccf_impl::forecast(
    int noutput_items, gr_vector_int &ninput_items_required) {
  /*
   * We will be called with noutput_items/2 if not enough input data is
   * avaliable, so we preemptively return the minimum input for that case.
   */
  if (noutput_items / 2 < work_len)
    ninput_items_required[0] = 1;
  else
    ninput_items_required[0] =
        std::max(1, (noutput_items - work_len) / stride + 1);
}

inline void polyphase_synt_filter_ccf_impl::method1(const gr_complex *in,
                                                    gr_complex *out) {
  for (int i = 0; i < taps_len; i += vlen)
    for (int j = 0; j < vlen; j++)
      out[i + j] += taps[i + j] * in[j];
}

inline void polyphase_synt_filter_ccf_impl::method2(const gr_complex *in,
                                                    gr_complex *out) {
  assert(vlen % 4 == 0);

  const float *t = taps;
  for (int i = 0; i < taps_len; i += vlen)
    for (int j = 0; j < vlen; j += 4) {
      __m128 c = _mm_load_ps(t);                           // t0, t1, t2, t3
      __m128 d = _mm_shuffle_ps(c, c, 0x50);               // t0, t0, t1, t1
      __m128 f = _mm_load_ps((const float *)(in + j));     // r0, i0, r1, i1
      __m128 e = _mm_shuffle_ps(c, c, 0xfa);               // t2, t2, t3, t3
      __m128 g = _mm_load_ps((const float *)(in + j + 2)); // r2, i2, r3, i3
      f = _mm_mul_ps(d, f);
      __m128 a = _mm_loadu_ps((float *)(out));
      g = _mm_mul_ps(e, g);
      __m128 b = _mm_loadu_ps((float *)(out + 2));
      a = _mm_add_ps(a, f);
      b = _mm_add_ps(b, g);
      _mm_storeu_ps((float *)(out), a);
      _mm_storeu_ps((float *)(out + 2), b);
      t += 4;
      out += 4;
    }
}

inline void polyphase_synt_filter_ccf_impl::method_vlen20(const gr_complex *in,
                                                          gr_complex *out) {
  assert(vlen == 20);
  const int vlen = 20;

  const float *t = taps;
  for (int i = 0; i < taps_len; i += vlen) {
    __m128 t0 = _mm_loadu_ps(t);                       // t0, t1, t2, t3
    __m128 a0 = _mm_loadu_ps((const float *)(in));     // r0, i0, r1, i1
    __m128 a1 = _mm_loadu_ps((const float *)(in + 2)); // r0, i0, r1, i1
    __m128 b0 = _mm_loadu_ps((const float *)(out));
    __m128 b1 = _mm_loadu_ps((const float *)(out + 2));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 2), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 4);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 4)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 6)); // r0, i0, r1, i1
    b0 = _mm_loadu_ps((const float *)(out + 4));
    b1 = _mm_loadu_ps((const float *)(out + 6));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 4), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 6), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 8);                    // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 8));  // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 10)); // r0, i0, r1, i1
    b0 = _mm_loadu_ps((const float *)(out + 8));
    b1 = _mm_loadu_ps((const float *)(out + 10));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 8), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 10), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 12);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 12)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 14)); // r0, i0, r1, i1
    b0 = _mm_loadu_ps((const float *)(out + 12));
    b1 = _mm_loadu_ps((const float *)(out + 14));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 12), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 14), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 16);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 16)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 18)); // r0, i0, r1, i1
    b0 = _mm_loadu_ps((const float *)(out + 16));
    b1 = _mm_loadu_ps((const float *)(out + 18));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 16), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 18), _mm_add_ps(a1, b1));

    t += 20;
    out += 20;
  }
}

inline void polyphase_synt_filter_ccf_impl::method_vlen25(const gr_complex *in,
                                                          gr_complex *out) {
  assert(vlen == 25);
  const int vlen = 25;

  const float *t = taps;
  for (int i = 0; i < taps_len; i += vlen) {
    __m128 t0 = _mm_loadu_ps(t);                       // t0, t1, t2, t3
    __m128 a0 = _mm_loadu_ps((const float *)(in));     // r0, i0, r1, i1
    __m128 a1 = _mm_loadu_ps((const float *)(in + 2)); // r0, i0, r1, i1
    __m128 b0 = _mm_loadu_ps((const float *)(out));
    __m128 b1 = _mm_loadu_ps((const float *)(out + 2));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 2), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 4);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 4)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 6)); // r0, i0, r1, i1
    b0 = _mm_loadu_ps((const float *)(out + 4));
    b1 = _mm_loadu_ps((const float *)(out + 6));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 4), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 6), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 8);                    // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 8));  // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 10)); // r0, i0, r1, i1
    b0 = _mm_loadu_ps((const float *)(out + 8));
    b1 = _mm_loadu_ps((const float *)(out + 10));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 8), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 10), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 12);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 12)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 14)); // r0, i0, r1, i1
    b0 = _mm_loadu_ps((const float *)(out + 12));
    b1 = _mm_loadu_ps((const float *)(out + 14));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 12), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 14), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 16);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 16)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 18)); // r0, i0, r1, i1
    b0 = _mm_loadu_ps((const float *)(out + 16));
    b1 = _mm_loadu_ps((const float *)(out + 18));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 16), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 18), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 20);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 20)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 22)); // r0, i0, r1, i1
    b0 = _mm_loadu_ps((const float *)(out + 20));
    b1 = _mm_loadu_ps((const float *)(out + 22));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 20), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 22), _mm_add_ps(a1, b1));

    t0 = _mm_load_ss(t + 24);                    // t0, 0, 0, 0
    a0 = _mm_loadu_ps((const float *)(in + 24)); // r0, i0, x, x
    b0 = _mm_loadu_ps((const float *)(out + 24));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, 0, 0
    _mm_storeu_ps((float *)(out + 24), _mm_add_ps(a0, b0));

    t += 25;
    out += 25;
  }
}

inline void polyphase_synt_filter_ccf_impl::method_vlen30(const gr_complex *in,
                                                          gr_complex *out) {
  assert(vlen == 30);
  const int vlen = 30;

  const float *t = taps;
  for (int i = 0; i < taps_len; i += vlen) {
    __m128 t0 = _mm_loadu_ps(t);                       // t0, t1, t2, t3
    __m128 a0 = _mm_loadu_ps((const float *)(in));     // r0, i0, r1, i1
    __m128 a1 = _mm_loadu_ps((const float *)(in + 2)); // r2, i2, r3, i3
    __m128 b0 = _mm_loadu_ps((const float *)(out));
    __m128 b1 = _mm_loadu_ps((const float *)(out + 2));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 2), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 4);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 4)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 6)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 4));
    b1 = _mm_loadu_ps((const float *)(out + 6));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 4), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 6), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 8);                    // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 8));  // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 10)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 8));
    b1 = _mm_loadu_ps((const float *)(out + 10));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 8), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 10), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 12);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 12)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 14)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 12));
    b1 = _mm_loadu_ps((const float *)(out + 14));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 12), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 14), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 16);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 16)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 18)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 16));
    b1 = _mm_loadu_ps((const float *)(out + 18));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 16), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 18), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 20);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 20)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 22)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 20));
    b1 = _mm_loadu_ps((const float *)(out + 22));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 20), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 22), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 24);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 24)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 26)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 24));
    b1 = _mm_loadu_ps((const float *)(out + 26));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 24), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 26), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 28);                   // t0, t1
    a0 = _mm_loadu_ps((const float *)(in + 28)); // r0, i0, r1, i1
    b0 = _mm_loadu_ps((const float *)(out + 28));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    _mm_storeu_ps((float *)(out + 28), _mm_add_ps(a0, b0));

    t += vlen;
    out += vlen;
  }
}

inline void polyphase_synt_filter_ccf_impl::method_vlen40(const gr_complex *in,
                                                          gr_complex *out) {
  assert(vlen == 40);
  const int vlen = 40;

  const float *t = taps;
  for (int i = 0; i < taps_len; i += vlen) {
    __m128 t0 = _mm_loadu_ps(t);                       // t0, t1, t2, t3
    __m128 a0 = _mm_loadu_ps((const float *)(in));     // r0, i0, r1, i1
    __m128 a1 = _mm_loadu_ps((const float *)(in + 2)); // r2, i2, r3, i3
    __m128 b0 = _mm_loadu_ps((const float *)(out));
    __m128 b1 = _mm_loadu_ps((const float *)(out + 2));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 2), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 4);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 4)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 6)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 4));
    b1 = _mm_loadu_ps((const float *)(out + 6));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 4), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 6), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 8);                    // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 8));  // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 10)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 8));
    b1 = _mm_loadu_ps((const float *)(out + 10));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 8), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 10), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 12);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 12)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 14)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 12));
    b1 = _mm_loadu_ps((const float *)(out + 14));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 12), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 14), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 16);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 16)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 18)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 16));
    b1 = _mm_loadu_ps((const float *)(out + 18));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 16), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 18), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 20);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 20)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 22)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 20));
    b1 = _mm_loadu_ps((const float *)(out + 22));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 20), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 22), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 24);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 24)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 26)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 24));
    b1 = _mm_loadu_ps((const float *)(out + 26));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 24), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 26), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 28);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 28)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 30)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 28));
    b1 = _mm_loadu_ps((const float *)(out + 30));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 28), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 30), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 32);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 32)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 34)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 32));
    b1 = _mm_loadu_ps((const float *)(out + 34));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 32), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 34), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 36);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 36)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 38)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 36));
    b1 = _mm_loadu_ps((const float *)(out + 38));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 36), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 38), _mm_add_ps(a1, b1));

    t += vlen;
    out += vlen;
  }
}

inline void polyphase_synt_filter_ccf_impl::method_vlen50(const gr_complex *in,
                                                          gr_complex *out) {
  assert(vlen == 50);
  const int vlen = 50;

  const float *t = taps;
  for (int i = 0; i < taps_len; i += vlen) {
    __m128 t0 = _mm_loadu_ps(t);                       // t0, t1, t2, t3
    __m128 a0 = _mm_loadu_ps((const float *)(in));     // r0, i0, r1, i1
    __m128 a1 = _mm_loadu_ps((const float *)(in + 2)); // r2, i2, r3, i3
    __m128 b0 = _mm_loadu_ps((const float *)(out));
    __m128 b1 = _mm_loadu_ps((const float *)(out + 2));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 2), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 4);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 4)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 6)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 4));
    b1 = _mm_loadu_ps((const float *)(out + 6));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 4), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 6), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 8);                    // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 8));  // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 10)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 8));
    b1 = _mm_loadu_ps((const float *)(out + 10));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 8), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 10), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 12);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 12)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 14)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 12));
    b1 = _mm_loadu_ps((const float *)(out + 14));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 12), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 14), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 16);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 16)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 18)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 16));
    b1 = _mm_loadu_ps((const float *)(out + 18));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 16), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 18), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 20);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 20)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 22)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 20));
    b1 = _mm_loadu_ps((const float *)(out + 22));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 20), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 22), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 24);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 24)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 26)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 24));
    b1 = _mm_loadu_ps((const float *)(out + 26));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 24), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 26), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 28);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 28)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 30)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 28));
    b1 = _mm_loadu_ps((const float *)(out + 30));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 28), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 30), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 32);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 32)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 34)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 32));
    b1 = _mm_loadu_ps((const float *)(out + 34));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 32), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 34), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 36);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 36)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 38)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 36));
    b1 = _mm_loadu_ps((const float *)(out + 38));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 36), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 38), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 40);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 40)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 42)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 40));
    b1 = _mm_loadu_ps((const float *)(out + 42));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 40), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 42), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 44);                   // t0, t1, t2, t3
    a0 = _mm_loadu_ps((const float *)(in + 44)); // r0, i0, r1, i1
    a1 = _mm_loadu_ps((const float *)(in + 46)); // r2, i2, r3, i3
    b0 = _mm_loadu_ps((const float *)(out + 44));
    b1 = _mm_loadu_ps((const float *)(out + 46));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    a1 = _mm_mul_ps(a1, _mm_shuffle_ps(t0, t0, 0xfa)); // t2, t2, t3, t3
    _mm_storeu_ps((float *)(out + 44), _mm_add_ps(a0, b0));
    _mm_storeu_ps((float *)(out + 46), _mm_add_ps(a1, b1));

    t0 = _mm_loadu_ps(t + 48);                   // t0, t1
    a0 = _mm_loadu_ps((const float *)(in + 48)); // r0, i0, r1, i1
    b0 = _mm_loadu_ps((const float *)(out + 48));
    a0 = _mm_mul_ps(a0, _mm_shuffle_ps(t0, t0, 0x50)); // t0, t0, t1, t1
    _mm_storeu_ps((float *)(out + 48), _mm_add_ps(a0, b0));

    t += vlen;
    out += vlen;
  }
}

int polyphase_synt_filter_ccf_impl::general_work(
    int noutput_items, gr_vector_int &ninput_items,
    gr_vector_const_void_star &input_items, gr_vector_void_star &output_items) {
  monitor_work(); // record extra performance values

  // assert(noutput_items >= work_len);
  if (noutput_items < work_len)
    return 0;

  const gr_complex *in = (const gr_complex *)input_items[0];
  gr_complex *out = (gr_complex *)output_items[0];
  int consumed =
      std::min((noutput_items - work_len) / stride + 1, ninput_items[0]);

  for (int i = 0; i < consumed; i++) {
    // clear memory ahead, piecewise
    std::memset(out + work_len - stride, 0, stride * sizeof(gr_complex));

    // add the new products
    if (vlen == 50)
      method_vlen50(in, out);
    else if (vlen == 40)
      method_vlen40(in, out);
    else if (vlen == 30)
      method_vlen30(in, out);
    else if (vlen == 25)
      method_vlen25(in, out);
    else if (vlen == 20)
      method_vlen20(in, out);
    else if (vlen % 4 == 0)
      method2(in, out);
    else
      method1(in, out);

    in += vlen;
    out += stride;
  }

  consume(0, consumed);
  return consumed * stride;
}

} /* namespace marmote3 */
} /* namespace gr */
