/* -*- c++ -*- */
/*
 * Copyright 2019 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_RADAR_DETECTOR_IMPL_H
#define INCLUDED_MARMOTE3_RADAR_DETECTOR_IMPL_H

#include <emmintrin.h>
#include <marmote3/radar_detector.h>

namespace gr {
namespace marmote3 {

class radar_detector_impl : public radar_detector {
private:
  const int vlen;
  const float alpha;
  const float power;
  const int min_count;
  const int max_report;
  const bool debug;

  float min_level; // noise_level * (1.0f / alpha)
  int last_count;

  typedef struct sse_stat_t {
    __m128 level0;
    __m128 level1;
    __m128 level2;
    __m128 level3;
    __m128i above;
  } sse_stat_t;

  sse_stat_t *sse_stats;
  int sse_work(const float *in);

  typedef struct cpu_stat_t {
    float level;
    uint8_t above;
  } cpu_stat_t;

  cpu_stat_t *cpu_stats;
  int cpu_work(const float *in);

  typedef struct item_t {
    item_t(int offset, int magnitude) : offset(offset), magnitude(magnitude) {}

    unsigned int offset;
    unsigned int magnitude;
  } item_t;

  std::vector<item_t> temp_items;

  gr::thread::mutex mutex;
  unsigned long report_start;
  unsigned int report_count;
  std::vector<item_t> report_items;

public:
  radar_detector_impl(int vlen, float alpha, float power, int min_count,
                      int max_report, bool debug);
  ~radar_detector_impl();

  int work(int noutput_items, gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);

  void collect_block_report(ModemReport *modem_report) override;
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_RADAR_DETECTOR_IMPL_H */
