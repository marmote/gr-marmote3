/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_PACKET_DEBUG_IMPL_H
#define INCLUDED_MARMOTE3_PACKET_DEBUG_IMPL_H

#include <marmote3/packet_debug.h>

namespace gr {
namespace marmote3 {

class packet_debug_impl : public packet_debug {
private:
  const int itemsize;
  const int trig_type;
  const pmt::pmt_t trig_key;
  const long trig_val;
  const int print_len;

public:
  packet_debug_impl(int itemsize, int trig_type, const std::string &trig_key,
                    long trig_val, int print_len);
  ~packet_debug_impl();

  int work(int noutput_items, gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_PACKET_DEBUG_IMPL_H */
