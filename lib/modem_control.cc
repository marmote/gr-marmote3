/* -*- c++ -*- */
/*
 * Copyright 2018 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <marmote3/marmote3.pb.h>
#include <marmote3/modem_control.h>
#include <pmt/pmt.h>

namespace gr {
namespace marmote3 {

void modem_control::set_modem_setter(gr::feval_p *setter) {
  gr::thread::scoped_lock lock(mutex);
  modem_setter = setter;
}

void modem_control::set_center_freq(double hz) {
  gr::thread::scoped_lock lock(mutex);
  center_freq = hz;
}

void modem_control::set_samp_rate(double hz) {
  gr::thread::scoped_lock lock(mutex);
  samp_rate = hz;
}

void modem_control::set_rx_gain(float db) {
  gr::thread::scoped_lock lock(mutex);
  rx_gain = db;
  if (nominal_rx_gain == -1.0f)
    nominal_rx_gain = db;
}

void modem_control::set_tx_gain(float db) {
  gr::thread::scoped_lock lock(mutex);
  tx_gain = db;
  if (nominal_tx_gain == -1.0f)
    nominal_tx_gain = db;
}

void modem_control::set_tx_power(float db) {
  gr::thread::scoped_lock lock(mutex);
  tx_power = db;
}

void modem_control::set_invert_spectrum(int flag) {
  gr::thread::scoped_lock lock(mutex);
  invert_spectrum = flag != 0;
}

void modem_control::set_enable_recording(int flag) {
  gr::thread::scoped_lock lock(mutex);
  enable_recording = flag != 0;
}

void modem_control::set_num_channels(int num) {
  gr::thread::scoped_lock lock(mutex);
  num_channels = num;
}

void modem_control::set_num_subcarriers(int num) {
  gr::thread::scoped_lock lock(mutex);
  num_subcarriers = num;
}

std::string modem_control::get_alias() { return "modem_control"; }

void modem_control::collect_block_report(ModemReport *modem_report) {
  ModemControlReport *report = modem_report->mutable_modem_control();
  gr::thread::scoped_lock lock(mutex);
  report->set_center_freq(center_freq);
  report->set_samp_rate(samp_rate);
  report->set_rx_gain(rx_gain);
  report->set_tx_gain(tx_gain);
  report->set_tx_power(tx_power);
  report->set_invert_spectrum(invert_spectrum);
  report->set_enable_recording(enable_recording);
  report->set_num_channels(num_channels);
  report->set_num_subcarriers(num_subcarriers);
  report->set_nominal_rx_gain(nominal_rx_gain);
  report->set_nominal_tx_gain(nominal_tx_gain);

  double f = 4.0 / 5.0; // default, should always be the case
  if (num_channels > 0 && num_subcarriers >= num_channels) {
    f = ((double)num_channels) / num_subcarriers;
  }
  report->set_rf_bandwidth(samp_rate * f);
}

void modem_control::process_block_command(const ModemCommand *modem_command) {
  if (modem_command->has_modem_control()) {
    gr::feval_p *setter;
    {
      gr::thread::scoped_lock lock(mutex);
      setter = modem_setter;
    }
    if (setter == NULL) {
      std::cout << "modem_control: modem setter is not available\n";
      return;
    }

    const ModemControlCommand &command = modem_command->modem_control();

    if (command.has_center_freq() && !center_freq_var.empty()) {
      pmt::pmt_t fun = pmt::string_to_symbol("set_" + center_freq_var);
      pmt::pmt_t arg = pmt::from_double(command.center_freq().value());
      setter->calleval(pmt::make_tuple(fun, arg));
    }

    if (command.has_rf_bandwidth() && !samp_rate_var.empty()) {
      long new_rate = find_good_samp_rate(command.rf_bandwidth().value());
      if (std::round(new_rate) != std::round(samp_rate)) {
        pmt::pmt_t fun = pmt::string_to_symbol("set_" + samp_rate_var);
        pmt::pmt_t arg = pmt::from_long(new_rate);
        setter->calleval(pmt::make_tuple(fun, arg));
      }
    }

    if (command.has_rx_gain() && !rx_gain_var.empty()) {
      pmt::pmt_t fun = pmt::string_to_symbol("set_" + rx_gain_var);
      pmt::pmt_t arg = pmt::from_float(command.rx_gain().value());
      setter->calleval(pmt::make_tuple(fun, arg));
    }

    if (command.has_tx_gain() && !tx_gain_var.empty()) {
      pmt::pmt_t fun = pmt::string_to_symbol("set_" + tx_gain_var);
      pmt::pmt_t arg = pmt::from_float(command.tx_gain().value());
      setter->calleval(pmt::make_tuple(fun, arg));
    }

    if (command.has_tx_power() && !tx_power_var.empty()) {
      pmt::pmt_t fun = pmt::string_to_symbol("set_" + tx_power_var);
      pmt::pmt_t arg = pmt::from_float(command.tx_power().value());
      setter->calleval(pmt::make_tuple(fun, arg));
    }

    if (command.has_invert_spectrum() && !invert_spectrum_var.empty()) {
      pmt::pmt_t fun = pmt::string_to_symbol("set_" + invert_spectrum_var);
      pmt::pmt_t arg =
          pmt::from_long(command.invert_spectrum().value() ? 1 : 0);
      setter->calleval(pmt::make_tuple(fun, arg));
    }

    if (command.has_enable_recording() && !enable_recording_var.empty()) {
      pmt::pmt_t fun = pmt::string_to_symbol("set_" + enable_recording_var);
      pmt::pmt_t arg =
          pmt::from_long(command.enable_recording().value() ? 1 : 0);
      setter->calleval(pmt::make_tuple(fun, arg));
    }
  }
}

long modem_control::find_good_samp_rate(double rf_bandwidth) {
  if (rf_bandwidth >= 40e6)
    return 50e6;
  else if (rf_bandwidth >= 25e6)
    return 31.25e6;
  else if (rf_bandwidth >= 20e6)
    return 25e6;
  else if (rf_bandwidth >= 10e6)
    return 12.5e6;
  else if (rf_bandwidth >= 8e6)
    return 10e6;
  else // rf_bandwidth >= 5e6
    return 6.25e6;
}

} /* namespace marmote3 */
} /* namespace gr */
