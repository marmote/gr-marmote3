/* -*- c++ -*- */
/*
 * Copyright 2013-2018 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "packet_symb_mod_impl.h"
#include <algorithm>
#include <gnuradio/io_signature.h>

namespace gr {
namespace marmote3 {

// http://ecee.colorado.edu/~ecen4242/UMB/modulate.htm
// https://www.adv-radio-sci.net/13/73/2015/ars-13-73-2015.pdf
// http://www.hpl.hp.com/techreports/2001/HPL-2001-246.pdf

const gr_complex points_bpsk[2] = {
    gr_complex(1.0f, 0.0f),
    gr_complex(-1.0f, 0.0f),
};

const float sqrt_half = std::sqrt(1.0f / 2);
const gr_complex points_qpsk[4] = {
    gr_complex(sqrt_half, sqrt_half),
    gr_complex(-sqrt_half, sqrt_half),
    gr_complex(sqrt_half, -sqrt_half),
    gr_complex(-sqrt_half, -sqrt_half),
};

const float pi = 3.14159265358979f;
const float cos_pi_eighth = std::cos(pi / 8);
const float sin_pi_eighth = std::sin(pi / 8);
const gr_complex points_8psk[8] = {
    gr_complex(cos_pi_eighth, sin_pi_eighth),
    gr_complex(sin_pi_eighth, cos_pi_eighth),
    gr_complex(-cos_pi_eighth, sin_pi_eighth),
    gr_complex(-sin_pi_eighth, cos_pi_eighth),
    gr_complex(cos_pi_eighth, -sin_pi_eighth),
    gr_complex(sin_pi_eighth, -cos_pi_eighth),
    gr_complex(-cos_pi_eighth, -sin_pi_eighth),
    gr_complex(-sin_pi_eighth, -cos_pi_eighth),
};

const float sqrt_one_tenth = std::sqrt(1.0f / 10);
const float sqrt_nine_tenth = 3.0f * sqrt_one_tenth;
const gr_complex points_16qam[16] = {
    gr_complex(sqrt_nine_tenth, sqrt_nine_tenth),
    gr_complex(sqrt_one_tenth, sqrt_nine_tenth),
    gr_complex(-sqrt_nine_tenth, sqrt_nine_tenth),
    gr_complex(-sqrt_one_tenth, sqrt_nine_tenth),
    gr_complex(sqrt_nine_tenth, sqrt_one_tenth),
    gr_complex(sqrt_one_tenth, sqrt_one_tenth),
    gr_complex(-sqrt_nine_tenth, sqrt_one_tenth),
    gr_complex(-sqrt_one_tenth, sqrt_one_tenth),
    gr_complex(sqrt_nine_tenth, -sqrt_nine_tenth),
    gr_complex(sqrt_one_tenth, -sqrt_nine_tenth),
    gr_complex(-sqrt_nine_tenth, -sqrt_nine_tenth),
    gr_complex(-sqrt_one_tenth, -sqrt_nine_tenth),
    gr_complex(sqrt_nine_tenth, -sqrt_one_tenth),
    gr_complex(sqrt_one_tenth, -sqrt_one_tenth),
    gr_complex(-sqrt_nine_tenth, -sqrt_one_tenth),
    gr_complex(-sqrt_one_tenth, -sqrt_one_tenth),
};

const float unit_64qam_1 = std::sqrt(1.0f / 42);
const float unit_64qam_3 = 3.0f * unit_64qam_1;
const float unit_64qam_5 = 5.0f * unit_64qam_1;
const float unit_64qam_7 = 7.0f * unit_64qam_1;
const gr_complex points_64qam[64] = {
    gr_complex(unit_64qam_7, unit_64qam_7),
    gr_complex(unit_64qam_5, unit_64qam_7),
    gr_complex(unit_64qam_1, unit_64qam_7),
    gr_complex(unit_64qam_3, unit_64qam_7),
    gr_complex(-unit_64qam_7, unit_64qam_7),
    gr_complex(-unit_64qam_5, unit_64qam_7),
    gr_complex(-unit_64qam_1, unit_64qam_7),
    gr_complex(-unit_64qam_3, unit_64qam_7),

    gr_complex(unit_64qam_7, unit_64qam_5),
    gr_complex(unit_64qam_5, unit_64qam_5),
    gr_complex(unit_64qam_1, unit_64qam_5),
    gr_complex(unit_64qam_3, unit_64qam_5),
    gr_complex(-unit_64qam_7, unit_64qam_5),
    gr_complex(-unit_64qam_5, unit_64qam_5),
    gr_complex(-unit_64qam_1, unit_64qam_5),
    gr_complex(-unit_64qam_3, unit_64qam_5),

    gr_complex(unit_64qam_7, unit_64qam_1),
    gr_complex(unit_64qam_5, unit_64qam_1),
    gr_complex(unit_64qam_1, unit_64qam_1),
    gr_complex(unit_64qam_3, unit_64qam_1),
    gr_complex(-unit_64qam_7, unit_64qam_1),
    gr_complex(-unit_64qam_5, unit_64qam_1),
    gr_complex(-unit_64qam_1, unit_64qam_1),
    gr_complex(-unit_64qam_3, unit_64qam_1),

    gr_complex(unit_64qam_7, unit_64qam_3),
    gr_complex(unit_64qam_5, unit_64qam_3),
    gr_complex(unit_64qam_1, unit_64qam_3),
    gr_complex(unit_64qam_3, unit_64qam_3),
    gr_complex(-unit_64qam_7, unit_64qam_3),
    gr_complex(-unit_64qam_5, unit_64qam_3),
    gr_complex(-unit_64qam_1, unit_64qam_3),
    gr_complex(-unit_64qam_3, unit_64qam_3),

    gr_complex(unit_64qam_7, -unit_64qam_7),
    gr_complex(unit_64qam_5, -unit_64qam_7),
    gr_complex(unit_64qam_1, -unit_64qam_7),
    gr_complex(unit_64qam_3, -unit_64qam_7),
    gr_complex(-unit_64qam_7, -unit_64qam_7),
    gr_complex(-unit_64qam_5, -unit_64qam_7),
    gr_complex(-unit_64qam_1, -unit_64qam_7),
    gr_complex(-unit_64qam_3, -unit_64qam_7),

    gr_complex(unit_64qam_7, -unit_64qam_5),
    gr_complex(unit_64qam_5, -unit_64qam_5),
    gr_complex(unit_64qam_1, -unit_64qam_5),
    gr_complex(unit_64qam_3, -unit_64qam_5),
    gr_complex(-unit_64qam_7, -unit_64qam_5),
    gr_complex(-unit_64qam_5, -unit_64qam_5),
    gr_complex(-unit_64qam_1, -unit_64qam_5),
    gr_complex(-unit_64qam_3, -unit_64qam_5),

    gr_complex(unit_64qam_7, -unit_64qam_1),
    gr_complex(unit_64qam_5, -unit_64qam_1),
    gr_complex(unit_64qam_1, -unit_64qam_1),
    gr_complex(unit_64qam_3, -unit_64qam_1),
    gr_complex(-unit_64qam_7, -unit_64qam_1),
    gr_complex(-unit_64qam_5, -unit_64qam_1),
    gr_complex(-unit_64qam_1, -unit_64qam_1),
    gr_complex(-unit_64qam_3, -unit_64qam_1),

    gr_complex(unit_64qam_7, -unit_64qam_3),
    gr_complex(unit_64qam_5, -unit_64qam_3),
    gr_complex(unit_64qam_1, -unit_64qam_3),
    gr_complex(unit_64qam_3, -unit_64qam_3),
    gr_complex(-unit_64qam_7, -unit_64qam_3),
    gr_complex(-unit_64qam_5, -unit_64qam_3),
    gr_complex(-unit_64qam_1, -unit_64qam_3),
    gr_complex(-unit_64qam_3, -unit_64qam_3),
};

int encode_bpsk(const unsigned char *bytes, int len, gr_complex *symbs) {
  assert(len >= 0);
  for (int i = 0; i < len; i++) {
    unsigned int a = *(bytes++);
    *(symbs++) = points_bpsk[a & 0x1];
    *(symbs++) = points_bpsk[(a >> 1) & 0x1];
    *(symbs++) = points_bpsk[(a >> 2) & 0x1];
    *(symbs++) = points_bpsk[(a >> 3) & 0x1];
    *(symbs++) = points_bpsk[(a >> 4) & 0x1];
    *(symbs++) = points_bpsk[(a >> 5) & 0x1];
    *(symbs++) = points_bpsk[(a >> 6) & 0x1];
    *(symbs++) = points_bpsk[(a >> 7) & 0x1];
  }
  return 8 * len;
}

int encode_qpsk(const unsigned char *bytes, int len, gr_complex *symbs) {
  assert(len >= 0);
  for (int i = 0; i < len; i++) {
    unsigned int a = *(bytes++);
    *(symbs++) = points_qpsk[a & 0x3];
    *(symbs++) = points_qpsk[(a >> 2) & 0x3];
    *(symbs++) = points_qpsk[(a >> 4) & 0x3];
    *(symbs++) = points_qpsk[(a >> 6) & 0x3];
  }
  return 4 * len;
}

int encode_8psk(const unsigned char *bytes, int len, gr_complex *symbs) {
  assert(len >= 0);
  if (len % 3 != 0) {
    std::cout << "####### encode_8psk: invalid number of bytes\n";
    return 0;
  }

  for (int i = 0; i < len; i += 3) {
    unsigned int a = *(bytes++);
    a += ((unsigned int)*(bytes++)) << 8;
    a += ((unsigned int)*(bytes++)) << 16;
    *(symbs++) = points_8psk[a & 0x7];
    *(symbs++) = points_8psk[(a >> 3) & 0x7];
    *(symbs++) = points_8psk[(a >> 6) & 0x7];
    *(symbs++) = points_8psk[(a >> 9) & 0x7];
    *(symbs++) = points_8psk[(a >> 12) & 0x7];
    *(symbs++) = points_8psk[(a >> 15) & 0x7];
    *(symbs++) = points_8psk[(a >> 18) & 0x7];
    *(symbs++) = points_8psk[(a >> 21) & 0x7];
  }
  return 8 * (len / 3);
}

int encode_16qam(const unsigned char *bytes, int len, gr_complex *symbs) {
  assert(len >= 0);
  for (int i = 0; i < len; i++) {
    unsigned int a = *(bytes++);
    *(symbs++) = points_16qam[a & 0xf];
    *(symbs++) = points_16qam[(a >> 4) & 0xf];
  }
  return 2 * len;
}

int encode_64qam(const unsigned char *bytes, int len, gr_complex *symbs) {
  assert(len >= 0);
  if (len % 3 != 0) {
    std::cout << "####### encode_64qam: invalid number of bytes\n";
    return 0;
  }

  for (int i = 0; i < len; i += 3) {
    unsigned int a = *(bytes++);
    a += ((unsigned int)*(bytes++)) << 8;
    a += ((unsigned int)*(bytes++)) << 16;
    *(symbs++) = points_64qam[a & 0x3f];
    *(symbs++) = points_64qam[(a >> 6) & 0x3f];
    *(symbs++) = points_64qam[(a >> 12) & 0x3f];
    *(symbs++) = points_64qam[(a >> 18) & 0x3f];
  }
  return 4 * (len / 3);
}

packet_symb_mod::sptr packet_symb_mod::make(modulation_t mod,
                                            int max_output_len) {
  return gnuradio::get_initial_sptr(
      new packet_symb_mod_impl(mod, max_output_len));
}

packet_symb_mod_impl::packet_symb_mod_impl(modulation_t mod, int max_output_len)
    : tagged_stream_block2("packet_symb_mod",
                           gr::io_signature::make(1, 1, sizeof(unsigned char)),
                           gr::io_signature::make(1, 1, sizeof(gr_complex)),
                           max_output_len),
      mod(mod) {
  if (mod != MOD_BPSK && mod != MOD_QPSK && mod != MOD_8PSK &&
      mod != MOD_16QAM && mod != MOD_64QAM && mod != MOD_MCS)
    throw std::invalid_argument("packet_symb_mod: invalid modulation");
}

packet_symb_mod_impl::~packet_symb_mod_impl() {}

int packet_symb_mod_impl::work(int noutput_items, gr_vector_int &ninput_items,
                               gr_vector_const_void_star &input_items,
                               gr_vector_void_star &output_items) {
  modulation_t mod2 = mod;
  if (mod2 == MOD_MCS) {
    if (!has_input_long(0, PMT_PACKET_MCS)) {
      std::cout << "####### packet_symb_mod: MCS is not set\n";
      return 0;
    }
    mod2 = get_mcscheme(get_input_long(0, PMT_PACKET_MCS, -1)).mod;
  }

  int len = ninput_items[0];
  if (8 * len > noutput_items * get_modulation_order(mod2)) {
    std::cout << "####### packet_symb_mod: output buffer too short\n";
    return 0;
  }

  const unsigned char *bytes = (const unsigned char *)input_items[0];
  gr_complex *symbs = (gr_complex *)output_items[0];

  switch (mod2) {
  case MOD_BPSK:
    return encode_bpsk(bytes, len, symbs);
  case MOD_QPSK:
    return encode_qpsk(bytes, len, symbs);
  case MOD_8PSK:
    return encode_8psk(bytes, len, symbs);
  case MOD_16QAM:
    return encode_16qam(bytes, len, symbs);
  case MOD_64QAM:
    return encode_64qam(bytes, len, symbs);
  default:
    std::cout << "####### packet_symb_mod: invalid modulation\n";
    return 0;
  }
} // namespace marmote3

} // namespace marmote3
} /* namespace gr */
