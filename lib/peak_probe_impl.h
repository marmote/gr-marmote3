/* -*- c++ -*- */
/*
 * Copyright 2013, 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_PEAK_PROBE_IMPL_H
#define INCLUDED_MARMOTE3_PEAK_PROBE_IMPL_H

#include <gnuradio/thread/thread.h>
#include <marmote3/peak_probe.h>

namespace gr {
namespace marmote3 {

class peak_probe_impl : public peak_probe {
private:
  float curr_peak; // for old method

  double energy; // total energy
  float maximum; // max energy
  long samples;  // number of samples
  gr::thread::mutex mutex;

public:
  peak_probe_impl();
  ~peak_probe_impl();

  int work(int noutput_items, gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items) override;

  float get_peak() override;

  void collect_block_report(ModemReport *modem_report) override;
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_PEAK_PROBE_IMPL_H */
