/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "polyphase_rotator_vcc_impl.h"
#include <cmath>
#include <gnuradio/io_signature.h>
#include <volk/volk.h>

namespace gr {
namespace marmote3 {

polyphase_rotator_vcc::sptr
polyphase_rotator_vcc::make(int vlen, const std::vector<int> &map, int stride) {
  return gnuradio::get_initial_sptr(
      new polyphase_rotator_vcc_impl(vlen, map, stride));
}

polyphase_rotator_vcc_impl::polyphase_rotator_vcc_impl(
    int vlen, const std::vector<int> &map, int stride)
    : gr::sync_block(
          "polyphase_rotator_vcc",
          gr::io_signature::make(1, 1, sizeof(gr_complex) * map.size()),
          gr::io_signature::make(1, 1, sizeof(gr_complex) * map.size())) {
  if (vlen <= 0)
    throw std::invalid_argument(
        "polyphase_rotator_vcc: vector length must be positive");

  if (map.size() <= 0)
    throw std::invalid_argument("polyphase_rotator_vcc: map cannot be empty");

  for (int i = 0; i < map.size(); i++)
    if (map[i] < 0 || map[i] >= vlen)
      throw std::invalid_argument("polyphase_rotator_vcc: invalid map entry");

  stride = stride % vlen;
  if (stride < 0)
    stride += vlen;

  period = 1;
  while ((period * stride) % vlen != 0)
    period += 1;

  for (int i = 0; i < period; i++)
    for (int j = 0; j < map.size(); j++) {
      int a = (i * map[j] * stride) % vlen;
      double f = (2.0 * M_PI * a) / vlen;
      rotations.push_back(gr_complex(std::cos(f), std::sin(f)));
    }

  set_output_multiple(period);
}

polyphase_rotator_vcc_impl::~polyphase_rotator_vcc_impl() {}

int polyphase_rotator_vcc_impl::work(int noutput_items,
                                     gr_vector_const_void_star &input_items,
                                     gr_vector_void_star &output_items) {
  const gr_complex *in = (const gr_complex *)input_items[0];
  gr_complex *out = (gr_complex *)output_items[0];

  int s = rotations.size();
  const gr_complex *rot = rotations.data();

  for (int n = 0; n < noutput_items; n += period) {
    volk_32fc_x2_multiply_32fc(out, in, rot, s);
    out += s;
    in += s;
  }

  return noutput_items;
}

} /* namespace marmote3 */
} /* namespace gr */
