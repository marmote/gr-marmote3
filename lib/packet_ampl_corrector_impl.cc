/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "packet_ampl_corrector_impl.h"
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <volk/volk.h>

namespace gr {
namespace marmote3 {

packet_ampl_corrector::sptr packet_ampl_corrector::make(int block_len,
                                                        int max_output_len) {
  return gnuradio::get_initial_sptr(
      new packet_ampl_corrector_impl(block_len, max_output_len));
}

packet_ampl_corrector_impl::packet_ampl_corrector_impl(int block_len,
                                                       int max_output_len)
    : tagged_stream_block2("packet_ampl_corrector",
                           gr::io_signature::make(1, 1, sizeof(gr_complex)),
                           gr::io_signature::make(1, 1, sizeof(gr_complex)),
                           max_output_len),
      block_len(block_len) {}

packet_ampl_corrector_impl::~packet_ampl_corrector_impl() {}

float packet_ampl_corrector_impl::get_rms(const gr_complex *in, int len) {
  float energy;
  volk_32f_x2_dot_prod_32f(&energy, (const float *)in, (const float *)in,
                           2 * len);
  return std::sqrt(std::max(energy, 1e-9f) / len);
}

int packet_ampl_corrector_impl::work(int noutput_items,
                                     gr_vector_int &ninput_items,
                                     gr_vector_const_void_star &input_items,
                                     gr_vector_void_star &output_items) {
  int len = ninput_items[0];
  if (len > noutput_items) {
    std::cout << "####### packet_ampl_corrector: output buffer is too small\n";
    return 0;
  }

  const gr_complex *in = (const gr_complex *)input_items[0];
  gr_complex *out = (gr_complex *)output_items[0];

  int pos;
  for (pos = 0; pos + 2 * block_len <= len; pos += block_len) {
    float s = 1.0f / get_rms(in, block_len);
    volk_32f_s32f_multiply_32f((float *)out, (const float *)in, s,
                               2 * block_len);
    in += block_len;
    out += block_len;
  }

  float s = 1.0f / get_rms(in, len - pos);
  volk_32f_s32f_multiply_32f((float *)out, (const float *)in, s,
                             2 * (len - pos));

  return len;
}

} /* namespace marmote3 */
} /* namespace gr */
