/* -*- c++ -*- */
/*
 * Copyright 2013-2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <cassert>
#include <iostream>
#include <marmote3/fec_conv_xcoder.h>
#include <stdexcept>

#ifdef __SSE2__
#include <smmintrin.h>
#include <tmmintrin.h>
#endif

namespace gr {
namespace marmote3 {

// ------- Encoder

// http://www.dj5hg.de/code/convolutional_codes.pdf
static const std::vector<int> VITERBI27 = {0x4F, 0x6D};
static const std::vector<int> VITERBI28 = {0xB7, 0xFD};
static const std::vector<int> VITERBI29 = {0x12F, 0x1ED};
static const std::vector<int> VITERBI210 = {0x23F, 0x36D};
static const std::vector<int> VITERBI37 = {0x4F, 0x57, 0x6D};
static const std::vector<int> VITERBI38 = {0x8B, 0xBF, 0xE5};
static const std::vector<int> VITERBI39 = {0x14B, 0x1BF, 0x1C9};
static const std::vector<int> VITERBI310 = {0x27B, 0x2BD, 0x379};
static const std::vector<int> VITERBI47 = {0x49, 0x5D, 0x6B, 0x7F};
static const std::vector<int> VITERBI48 = {0xA7, 0xB9, 0xDB, 0xFD};
static const std::vector<int> VITERBI49 = {0x13F, 0x15D, 0x1BB, 0x1E9};
static const std::vector<int> VITERBI410 = {0x27D, 0x2EF, 0x35B, 0x391};
static const std::vector<int> VITERBI67 = {0x4B, 0x4F, 0x57, 0x6D, 0x75, 0x7D};
static const std::vector<int> VITERBI68 = {0x95, 0xA7, 0xB9, 0xDB, 0xEF, 0xFD};
static const std::vector<int> VITERBI69 = {0x12D, 0x153, 0x16F,
                                           0x1BD, 0x1DB, 0x1F1};
static const std::vector<int> VITERBI610 = {0x27D, 0x2BF, 0x2EF,
                                            0x32D, 0x35B, 0x393};

std::vector<int> fec_conv_encoder::get_standard_polys(int rate, int k) {
  if (rate == 2 && k == 7)
    return VITERBI27;
  else if (rate == 2 && k == 8)
    return VITERBI28;
  else if (rate == 2 && k == 9)
    return VITERBI29;
  else if (rate == 2 && k == 10)
    return VITERBI210;
  else if (rate == 3 && k == 7)
    return VITERBI37;
  else if (rate == 3 && k == 8)
    return VITERBI38;
  else if (rate == 3 && k == 9)
    return VITERBI39;
  else if (rate == 3 && k == 10)
    return VITERBI310;
  else if (rate == 4 && k == 7)
    return VITERBI47;
  else if (rate == 4 && k == 8)
    return VITERBI48;
  else if (rate == 4 && k == 9)
    return VITERBI49;
  else if (rate == 4 && k == 10)
    return VITERBI410;
  else if (rate == 6 && k == 7)
    return VITERBI67;
  else if (rate == 6 && k == 8)
    return VITERBI68;
  else if (rate == 6 && k == 9)
    return VITERBI69;
  else if (rate == 6 && k == 10)
    return VITERBI610;
  else
    throw std::invalid_argument(
        "rate must be 2, 3, 4 or 6 and k must be 7, 8, 9 or 10");
}

fec_conv_encoder::fec_conv_encoder(const std::vector<int> &polys, int k)
    : polys(polys), k(k) {
  if (k <= 0)
    throw std::out_of_range("fec_conv_decoder: k must be positive");

  if (polys.size() == 0)
    throw std::out_of_range("fec_conv_encoder: invalid polynomials");

  for (int i = 0; i < polys.size(); i++)
    if (polys[i] >= (1 << k))
      throw std::out_of_range("fec_conv_encoder: invalid polynomials");
}

int fec_conv_encoder::get_code_len(int data_len) const {
  return (data_len + k - 1) * polys.size();
}

int fec_conv_encoder::get_data_len(int code_len) const {
  return code_len / polys.size() - k + 1;
}

void fec_conv_encoder::encode(const uint8_t *data_bits, int data_len,
                              uint8_t *code_bits) const {
  int sr = 0;
  for (int i = 0; i < data_len + k - 1; ++i) {
    uint8_t bit = i < data_len ? *(data_bits++) : 0;
    assert(bit == 0 || bit == 1);

    sr = (sr << 1) | (bit & 0x01);
    for (int j = 0; j < polys.size(); ++j)
      *(code_bits++) = __builtin_parity(sr & polys[j]);
  }
}

std::vector<uint8_t>
fec_conv_encoder::encode(const std::vector<uint8_t> &data_bits) const {
  std::vector<uint8_t> code_bits(get_code_len(data_bits.size()), 0);
  encode(data_bits.data(), data_bits.size(), code_bits.data());
  return code_bits;
}

int fec_conv_encoder::get_code_len_tb(int data_len) const {
  return data_len * polys.size();
}

int fec_conv_encoder::get_data_len_tb(int code_len) const {
  return code_len / polys.size();
}

void fec_conv_encoder::encode_tb(const uint8_t *data_bits, int data_len,
                                 uint8_t *code_bits) const {
  if (data_len < k - 1)
    throw std::invalid_argument("data length is too small");

  int sr = 0;
  for (int i = 0; i < k - 1; ++i) {
    sr = (sr << 1) | (data_bits[i] & 0x01);
  }

  for (int i = k - 1; i < data_len; ++i) {
    uint8_t bit = data_bits[i];
    assert(bit == 0 || bit == 1);

    sr = (sr << 1) | (bit & 0x01);
    for (int j = 0; j < polys.size(); ++j)
      *(code_bits++) = __builtin_parity(sr & polys[j]);
  }

  for (int i = 0; i < k - 1; ++i) {
    uint8_t bit = data_bits[i];
    assert(bit == 0 || bit == 1);

    sr = (sr << 1) | (bit & 0x01);
    for (int j = 0; j < polys.size(); ++j)
      *(code_bits++) = __builtin_parity(sr & polys[j]);
  }
}

std::vector<uint8_t>
fec_conv_encoder::encode_tb(const std::vector<uint8_t> &data_bits) const {
  std::vector<uint8_t> code_bits(get_code_len_tb(data_bits.size()), 0);
  encode_tb(data_bits.data(), data_bits.size(), code_bits.data());
  return code_bits;
}

// ------- Decoder

fec_conv_decoder::fec_conv_decoder(const std::vector<int> &polys, int k,
                                   int max_data_len)
    : rate(polys.size()), k(k), numstates(1 << (k - 1)),
      max_data_len(max_data_len) {
  if (k < 4)
    throw std::out_of_range("fec_conv_decoder: k must be at least 4");

  if (polys.size() == 0)
    throw std::out_of_range("fec_conv_decoder: invalid polynomials");

  for (int i = 0; i < polys.size(); i++)
    if (polys[i] >= (1 << k))
      throw std::out_of_range("fec_conv_decoder: invalid polynomials");

  if (max_data_len <= 0)
    throw std::out_of_range("fec_conv_decoder: invalid max data length");

  if (posix_memalign((void **)&metrics1, 16, numstates * sizeof(float)) != 0)
    throw std::bad_alloc();

  if (posix_memalign((void **)&metrics2, 16, numstates * sizeof(float)) != 0)
    throw std::bad_alloc();

  if (posix_memalign((void **)&branchtab, 16,
                     numstates / 2 * rate * sizeof(float)) != 0)
    throw std::bad_alloc();

  if (posix_memalign((void **)&decisions, 8, numstates * max_data_len) != 0)
    throw std::bad_alloc();

  old_metrics = metrics1;
  new_metrics = metrics2;

  for (int state = 0; state < numstates / 2; state++)
    for (int i = 0; i < rate; i++) {
      branchtab[state + i * numstates / 2] =
          (polys[i] < 0) ^ __builtin_parity((2 * state) & std::abs(polys[i]))
              ? 1.0f
              : -1.0f;
    }
}

fec_conv_decoder::~fec_conv_decoder() {
  free(metrics1);
  metrics1 = NULL;

  free(metrics2);
  metrics2 = NULL;

  free(branchtab);
  branchtab = NULL;

  free(decisions);
  decisions = NULL;
}

int fec_conv_decoder::get_code_len(int data_len) const {
  return (data_len + k - 1) * rate;
}

int fec_conv_decoder::get_data_len(int code_len) const {
  return code_len / rate - k + 1;
}

void fec_conv_decoder::butterfly(const float *symbols, uint8_t *cur_decision) {
#ifndef __SSE2__
  for (int state = 0; state < numstates / 2; state++) {
    float metric = 0.0f;
    for (int j = 0; j < rate; j++) {
      metric += branchtab[state + j * numstates / 2] * symbols[j];
    }

    float m0 = old_metrics[state] + metric;
    float m1 = old_metrics[state + numstates / 2] - metric;
    float m2 = old_metrics[state] - metric;
    float m3 = old_metrics[state + numstates / 2] + metric;

    unsigned char decision0 = m0 > m1 ? 1 : 0;
    unsigned char decision1 = m2 > m3 ? 1 : 0;

    new_metrics[2 * state] = decision0 ? m1 : m0;
    new_metrics[2 * state + 1] = decision1 ? m3 : m2;

    cur_decision[2 * state] = decision0;
    cur_decision[2 * state + 1] = decision1;
  }
#else
  assert(numstates % 8 == 0);
  __m128i zero = _mm_setzero_si128();
  __m128i ones = _mm_set1_epi8(1);

  for (int state = 0; state < numstates / 2; state += 4) {
    __m128 metric = _mm_setzero_ps();
    for (int j = 0; j < rate; j++) {
      __m128 symbol = _mm_set1_ps(symbols[j]);
      __m128 branch = _mm_load_ps(&branchtab[state + j * numstates / 2]);
      metric = _mm_add_ps(metric, _mm_mul_ps(branch, symbol));
    }

    __m128 m2 = _mm_loadu_ps(&old_metrics[state]);
    __m128 m3 = _mm_loadu_ps(&old_metrics[state + numstates / 2]);
    __m128 m0 = _mm_add_ps(m2, metric);
    __m128 m1 = _mm_sub_ps(m3, metric);
    m2 = _mm_sub_ps(m2, metric);
    m3 = _mm_add_ps(m3, metric);

    __m128 x0 = _mm_min_ps(m0, m1);
    __m128 x1 = _mm_min_ps(m2, m3);

    _mm_storeu_ps(&new_metrics[2 * state], _mm_unpacklo_ps(x0, x1));
    _mm_storeu_ps(&new_metrics[2 * state + 4], _mm_unpackhi_ps(x0, x1));

    __m128 y0 = _mm_sub_ps(m0, m1);
    __m128 y1 = _mm_sub_ps(m2, m3);

    __m128i z0 = _mm_cvtps_epi32(_mm_unpacklo_ps(y0, y1));
    __m128i z1 = _mm_cvtps_epi32(_mm_unpackhi_ps(y0, y1));

    __m128i u = _mm_packs_epi16(_mm_packs_epi32(z0, z1), zero);
    u = _mm_and_si128(_mm_cmpgt_epi8(u, zero), ones);

    _mm_storel_pi((__m64 *)(cur_decision + 2 * state), _mm_castsi128_ps(u));
  }
#endif

  // Swap old and new metrics
  float *tmp = old_metrics;
  old_metrics = new_metrics;
  new_metrics = tmp;
}

void fec_conv_decoder::decode(const float *code_bits, int data_len,
                              uint8_t *data_bits) {
  if (data_len > max_data_len)
    throw std::invalid_argument("data length is too large");

  old_metrics[0] = 0.0f; // start_state is 0
  for (int i = 1; i < numstates; i++)
    old_metrics[i] = 1e9f;

  uint8_t *cur_decision = decisions;
  for (int i = 0; i < k - 1; i++) {
    butterfly(code_bits, cur_decision);
    code_bits += rate;
  }

  for (int i = 0; i < data_len; i++) {
    butterfly(code_bits, cur_decision);
    code_bits += rate;
    cur_decision += numstates;
  }

  int end_state = 0;
  for (int i = data_len - 1; i >= 0; i--) {
    cur_decision -= numstates;

    uint8_t bit = cur_decision[end_state];
    data_bits[i] = bit;

    end_state = (end_state >> 1) | (bit << (k - 2));
  }
}

std::vector<uint8_t>
fec_conv_decoder::decode(const std::vector<float> &code_bits) {
  int data_len = get_data_len(code_bits.size());
  if (data_len > max_data_len || get_code_len(data_len) != code_bits.size())
    throw std::invalid_argument("code length is invalid");

  std::vector<uint8_t> data_bits(data_len, 0);
  decode(code_bits.data(), data_len, data_bits.data());
  return data_bits;
}

int fec_conv_decoder::get_code_len_tb(int data_len) const {
  return data_len * rate;
}

int fec_conv_decoder::get_data_len_tb(int code_len) const {
  return code_len / rate;
}

void fec_conv_decoder::decode_tb(const float *code_bits, int data_len,
                                 uint8_t *data_bits) {
  if (data_len > max_data_len)
    throw std::invalid_argument("data length is too large");
  else if (data_len < k - 1)
    throw std::invalid_argument("data length is too small");

  for (int i = 0; i < numstates; i++)
    old_metrics[i] = 0.0f;

  uint8_t *cur_decision = decisions;
  for (int b = 0; b < 2; b++) {
    const float *code_bits2 = code_bits;
    for (int i = 0; i < data_len; i++) {
      butterfly(code_bits2, cur_decision);
      code_bits2 += rate;
    }
  }

  for (int i = 0; i < data_len; i++) {
    butterfly(code_bits, cur_decision);
    code_bits += rate;
    cur_decision += numstates;
  }

  int end_state = 0;
  float min_metric = old_metrics[0];
  for (int state = 1; state < numstates; ++state) {
    if (old_metrics[state] < min_metric) {
      min_metric = old_metrics[state];
      end_state = state;
    }
  }

  for (int i = data_len - 1; i >= 0; i--) {
    cur_decision -= numstates;

    uint8_t bit = cur_decision[end_state];
    data_bits[i] = bit;

    end_state = (end_state >> 1) | (bit << (k - 2));
  }
}

std::vector<uint8_t>
fec_conv_decoder::decode_tb(const std::vector<float> &code_bits) {
  int data_len = get_data_len_tb(code_bits.size());
  if (data_len > max_data_len || get_code_len_tb(data_len) != code_bits.size())
    throw std::invalid_argument("code length is invalid");

  std::vector<uint8_t> data_bits(data_len, 0);
  decode_tb(code_bits.data(), data_len, data_bits.data());
  return data_bits;
}

} /* namespace marmote3 */
} /* namespace gr */
