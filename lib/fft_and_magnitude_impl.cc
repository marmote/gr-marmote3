/* -*- c++ -*- */
/*
 * Copyright 2019 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "fft_and_magnitude_impl.h"
#include <gnuradio/io_signature.h>
#include <volk/volk.h>

namespace gr {
namespace marmote3 {

fft_and_magnitude::sptr
fft_and_magnitude::make(int fft_size, int fft_stride, bool forward,
                        const std::vector<float> &window, bool shift) {
  return gnuradio::get_initial_sptr(
      new fft_and_magnitude_impl(fft_size, fft_stride, forward, window, shift));
}

fft_and_magnitude_impl::fft_and_magnitude_impl(int fft_size, int fft_stride,
                                               bool forward,
                                               const std::vector<float> &window,
                                               bool shift)
    : gr::block("fft_and_magnitude",
                gr::io_signature::make(1, 1, sizeof(gr_complex)),
                gr::io_signature::make(1, 1, fft_size * sizeof(float))),
      fft_size(fft_size), fft_stride(fft_stride), forward(forward),
      window(window), shift(shift), fft(fft_size, forward, 1) {
  if (fft_size <= 0)
    throw std::out_of_range("fft_and_magnitude: invalid fft size");

  if (fft_stride <= 0)
    throw std::out_of_range("fft_and_magnitude: invalid fft stride");

  if (window.size() != fft_size)
    throw std::out_of_range("fft_and_magnitude: invalid window length");

  set_relative_rate(1.0f / fft_stride);
}

fft_and_magnitude_impl::~fft_and_magnitude_impl() {}

void fft_and_magnitude_impl::forecast(int noutput_items,
                                      gr_vector_int &ninput_items_required) {
  ninput_items_required[0] =
      std::max(fft_size, fft_stride) + fft_stride * (noutput_items - 1);
}

inline void fft_and_magnitude_impl::process(const gr_complex *in, float *out) {
  gr_complex *ibuf = fft.get_inbuf();
  if (!forward && shift) {
    int offset = fft_size / 2;
    volk_32fc_32f_multiply_32fc(ibuf + (fft_size - offset), in, &window[0],
                                offset);
    volk_32fc_32f_multiply_32fc(ibuf, in + offset, &window[offset],
                                fft_size - offset);
  } else {
    volk_32fc_32f_multiply_32fc(ibuf, in, &window[0], fft_size);
  }

  fft.execute();

  const gr_complex *obuf = fft.get_outbuf();
  if (forward && shift) {
    int offset = fft_size / 2;
    volk_32fc_magnitude_squared_32f(out + offset, obuf, fft_size - offset);
    volk_32fc_magnitude_squared_32f(out, obuf + (fft_size - offset), offset);
  } else {
    volk_32fc_magnitude_squared_32f(out, obuf, fft_size);
  }
}

int fft_and_magnitude_impl::general_work(int noutput_items,
                                         gr_vector_int &ninput_items,
                                         gr_vector_const_void_star &input_items,
                                         gr_vector_void_star &output_items) {
  const gr_complex *in = (const gr_complex *)input_items[0];
  float *out = (float *)output_items[0];

  for (int i = 0; i < noutput_items; i++) {
    process(in, out);
    in += fft_stride;
    out += fft_size;
  }

  consume_each(noutput_items * fft_stride);
  return noutput_items;
}

} /* namespace marmote3 */
} /* namespace gr */
