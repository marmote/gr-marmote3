/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "packet_preamb_equalizer_impl.h"
#include <cmath>
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <marmote3/marmote3.pb.h>
#include <volk/volk.h>

namespace gr {
namespace marmote3 {

packet_preamb_equalizer::sptr packet_preamb_equalizer::make(
    const std::vector<gr_complex> &preamble, int samp_per_symb, int padding_len,
    header_t header, bool postamble, int max_output_len, float energy_limit,
    bool save_taps, bool frame_debug) {
  return gnuradio::get_initial_sptr(new packet_preamb_equalizer_impl(
      preamble, samp_per_symb, padding_len, header, postamble, max_output_len,
      energy_limit, save_taps, frame_debug));
}

packet_preamb_equalizer_impl::packet_preamb_equalizer_impl(
    const std::vector<gr_complex> &preamble, int samp_per_symb, int padding_len,
    header_t header, bool postamble, int max_output_len, float energy_limit,
    bool save_taps, bool frame_debug)
    : tagged_stream_block2("packet_preamb_equalizer",
                           gr::io_signature::make(1, 1, sizeof(gr_complex)),
                           gr::io_signature::make(1, 1, sizeof(gr_complex)),
                           max_output_len),
      preamble_samp(preamble.size()), padding_samp(samp_per_symb * padding_len),
      samp_per_symb(samp_per_symb), postamble(postamble),
      payload_len(max_output_len), energy_limit(energy_limit), header(header),
      save_taps(save_taps), frame_debug(frame_debug), preamble_full(preamble),
      preamble_fft(preamble_samp), preamble_dat(preamble_samp / samp_per_symb),
      forward(preamble_samp, true, 1), backward(preamble_samp, false, 1),
      taps(2 * padding_samp + 1), fir(samp_per_symb, taps),
      decoder(fec_conv_encoder::get_standard_polys(2, 9), 9, 32) {
  if (preamble.size() <= 0)
    throw std::invalid_argument(
        "packet_preamb_equalizer: preamble cannot be empty");

  if (samp_per_symb <= 0)
    throw std::invalid_argument(
        "packet_preamb_equalizer: samples per symbol must be positive");

  if ((preamble.size() % samp_per_symb) != 0)
    throw std::invalid_argument(
        "packet_preamb_equalizer: preamble length must be divisible by sps");

  if (max_output_len <= 0)
    throw std::invalid_argument(
        "packet_preamb_equalizer: output length must be positive");

  if (padding_len < 0 || taps.size() > preamble_samp)
    throw std::invalid_argument(
        "packet_preamb_equalizer: invalid padding length");

  if (energy_limit < 0.0f || energy_limit > 1.0f)
    throw std::invalid_argument(
        "packet_preamb_equalizer: invalid bin energy limit");

  if (header != HDR_NONE && header != HDR_SRC_MCS_LEN_CRC)
    throw std::invalid_argument("packet_preamb_equalizer: invalid header type");

  // NOTE: both forward and backward fft is multiplying the energy by
  // preamble_samp
  std::memcpy(forward.get_inbuf(), preamble.data(),
              sizeof(gr_complex) * preamble_samp);
  forward.execute();
  std::memcpy(preamble_fft.data(), forward.get_outbuf(),
              sizeof(gr_complex) * preamble_samp);

  for (int i = 0; i < preamble_samp; i++)
    preamble_fft[i] *= 1.0f / preamble_samp;

  preamble_energy = 0.0f;
  for (int i = 0; i < preamble_samp / samp_per_symb; i++) {
    gr_complex c = preamble[i * samp_per_symb];
    preamble_dat[i] = c;
    preamble_energy += std::norm(c);
  }
}

packet_preamb_equalizer_impl::~packet_preamb_equalizer_impl() {}

int packet_preamb_equalizer_impl::work(int noutput_items,
                                       gr_vector_int &ninput_items,
                                       gr_vector_const_void_star &input_items,
                                       gr_vector_void_star &output_items) {
  const gr_complex *input = (const gr_complex *)input_items[0];
  gr_complex *output = (gr_complex *)output_items[0];

  if (header == HDR_SRC_MCS_LEN_CRC) {
    return work_header_src_mcs_len_crc(input, ninput_items[0], output,
                                       noutput_items);
  } else {
    return work_header_none(input, ninput_items[0], output, noutput_items);
  }
}

void packet_preamb_equalizer_impl::collect_block_report(
    ModemReport *modem_report) {
  PreambleEqualizer *report = modem_report->mutable_preamble_equalizer();
  gr::thread::scoped_lock lock(mutex);

  // there are multiple instances, so we add them up
  report->set_total_packets(report->total_packets() + total_packets);
  if (header_crc_error != 0)
    report->set_header_crc_error(report->header_crc_error() + header_crc_error);
  if (invalid_mcs_index != 0)
    report->set_invalid_mcs_index(report->invalid_mcs_index() +
                                  invalid_mcs_index);
  if (invalid_data_len != 0)
    report->set_invalid_data_len(report->invalid_data_len() + invalid_data_len);
  if (frame_too_short != 0)
    report->set_frame_too_short(report->frame_too_short() + frame_too_short);
  if (frame_too_long != 0)
    report->set_frame_too_long(report->frame_too_long() + frame_too_long);
  if (frame_truncated != 0)
    report->set_frame_truncated(report->frame_truncated() + frame_truncated);

  total_packets = 0;
  header_crc_error = 0;
  invalid_mcs_index = 0;
  invalid_data_len = 0;
  frame_too_short = 0;
  frame_too_long = 0;
  frame_truncated = 0;
}

int packet_preamb_equalizer_impl::work_header_none(const gr_complex *input,
                                                   int input_len,
                                                   gr_complex *output,
                                                   int output_len) {
  if (input_len != padding_samp + preamble_samp + padding_samp +
                       output_len * samp_per_symb + padding_samp) {
    std::cout << "####### packet_preamb_equalizer: invalid input length\n";
    return 0;
  }

  const gr_complex *preamble_start;
  const gr_complex *payload_start;
  if (!postamble) {
    // filter delay is exactly padding_samp, so we start padding_samp early
    preamble_start = input;
    payload_start = input + padding_samp + preamble_samp;
  } else {
    preamble_start = input + padding_samp + output_len * samp_per_symb;
    payload_start = input;
  }

  analyze_preamble(preamble_start);

  fir.filterNdec(output, payload_start, output_len, samp_per_symb);

  if (frame_debug) {
    int sync_offset = find_sync_offset(preamble_start);

    std::stringstream msg;
    msg << "packet_preamb_equalizer: sync offset " << sync_offset << std::endl;
    std::cout << msg.str();
  }

  gr::thread::scoped_lock lock(mutex);
  total_packets += 1;

  return output_len;
}

int packet_preamb_equalizer_impl::work_header_src_mcs_len_crc(
    const gr_complex *input, int input_len, gr_complex *output,
    int output_len) {
  if (input_len < padding_samp + 40 * samp_per_symb + padding_samp +
                      preamble_samp + padding_samp) {
    std::cout << "####### packet_preamb_equalizer: invalid input length\n";
    return 0;
  }

  const gr_complex *header_start;
  const gr_complex *preamble_start;
  if (!postamble) {
    // filter delay is exactly padding_samp, so we start padding_samp early
    header_start = input;
    preamble_start = input + padding_samp + 40 * samp_per_symb;
  } else {
    header_start =
        input + input_len - (padding_samp + 40 * samp_per_symb + padding_samp);
    preamble_start = header_start - (preamble_samp + padding_samp);
  }

  // find_sync_offset(preamble_start);
  analyze_preamble(preamble_start);

  uint8_t bytes[4];
  decode_header(header_start, bytes, 4);

  gr::thread::scoped_lock lock(mutex);
  total_packets += 1;

  if (bytes[3] != (uint8_t)(bytes[0] + bytes[1] + bytes[2] + 73)) {
    header_crc_error += 1;
    return 0;
  }

  int src = bytes[0];
  int mcs = (bytes[1] >> 3) & 0x1f;
  if (mcs >= MCSCHEME_NUM) {
    invalid_mcs_index += 1;
    return 0;
  }

  int data_len = ((bytes[1] & 0x7) << 8) + bytes[2];
  if (data_len <= 0 || data_len % 2 != 0) {
    invalid_data_len += 1;
    return 0;
  }

  int data_symbols = get_mcscheme(mcs).get_symb_len(data_len);
  int input_symbols =
      (input_len - (padding_samp + 40 * samp_per_symb + padding_samp +
                    preamble_samp + padding_samp + padding_samp)) /
      samp_per_symb;

  if (frame_debug) {
    int sync_offset = find_sync_offset(preamble_start);

    std::stringstream msg;
    msg << "packet_preamb_equalizer: sync offset " << sync_offset << " input "
        << input_symbols << " data " << data_symbols << " symbols" << std::endl;
    std::cout << msg.str();
  }

  if (data_symbols > output_len) {
    frame_too_long += 1;
    return 0;
  }

  // expect half of the symbols to be present
  if (input_symbols < data_symbols / 2) {
    frame_too_short += 1;
    return 0;
  } else if (input_symbols < data_symbols)
    frame_truncated += 1;
  else if (input_symbols > data_symbols)
    input_symbols = data_symbols;

  if (!postamble) {
    fir.filterNdec(output,
                   input + padding_samp + 40 * samp_per_symb + padding_samp +
                       preamble_samp,
                   input_symbols, samp_per_symb);

    // we pad the missing symbols with zero
    if (input_symbols < data_symbols) {
      std::memset((void *)(output + input_symbols), 0,
                  sizeof(gr_complex) * (data_symbols - input_symbols));
    }
  } else {
    // TODO: test this padding and how it affects the RA code
    if (input_symbols < data_symbols) {
      std::memset(output, 0,
                  sizeof(gr_complex) * (data_symbols - input_symbols));
    }

    fir.filterNdec(output + (data_symbols - input_symbols),
                   preamble_start -
                       (input_symbols * samp_per_symb + padding_samp),
                   input_symbols, samp_per_symb);
  }

  set_output_long(PMT_PACKET_SRC, src);
  set_output_long(PMT_PACKET_MCS, mcs);
  set_output_long(PMT_DATA_LEN, data_len);

  return data_symbols;
}

/**
 * Takes a pointer to (padding_samp + preamble_samp + padding_samp) many
 * samples and finds the best time syncrhonization offset.
 */
int packet_preamb_equalizer_impl::find_sync_offset(const gr_complex *samples) {
  int best_offset = 0;
  float best_match = 0.0f;
  for (int i = 0; i <= padding_samp * 2; i++) {
    gr_complex m;
    volk_32fc_x2_conjugate_dot_prod_32fc(&m, samples + i, preamble_full.data(),
                                         preamble_full.size());
    float match = std::norm(m);
    if (match > best_match) {
      best_match = match;
      best_offset = i - padding_samp;
    }
  }

  return best_offset;
}

/**
 * Takes a pointer to (padding_samp + preamble_samp + padding_samp) many
 * samples and calculates the equalizer filter taps (and the preamble power
 * and SNR).
 */
void packet_preamb_equalizer_impl::analyze_preamble(const gr_complex *samples) {
  float avg_energy;
  volk_32f_x2_dot_prod_32f(&avg_energy, (const float *)(samples + padding_samp),
                           (const float *)(samples + padding_samp),
                           2 * preamble_samp);
  avg_energy /= preamble_samp;

  std::memcpy(forward.get_inbuf(), samples + padding_samp,
              sizeof(gr_complex) * preamble_samp);
  forward.execute();

  const gr_complex *bs = forward.get_outbuf();
  gr_complex *cs = backward.get_inbuf();

  float limit = avg_energy * energy_limit;
  for (int i = 0; i < preamble_samp; i++) {
    gr_complex a = preamble_fft[i];
    gr_complex b = bs[i];
    cs[i] = (a * std::conj(b)) / (std::norm(b) + limit);
  }

  backward.execute();
  bs = backward.get_outbuf();

  std::memcpy(taps.data(), bs + preamble_samp - padding_samp,
              sizeof(gr_complex) * padding_samp);
  std::memcpy(taps.data() + padding_samp, bs,
              sizeof(gr_complex) * (padding_samp + 1));
  fir.set_taps(taps);

  float noise = 1e-9f;
  for (int i = 0; i < preamble_samp / samp_per_symb; i++) {
    gr_complex d = fir.filter(samples + i * samp_per_symb);
    noise += std::norm(d - preamble_dat[i]);
  }

  set_output_float(PMT_EQU_PWR, 10.0f * std::log10(avg_energy));
  set_output_float(PMT_EQU_SNR, 10.0f * std::log10(preamble_energy / noise));

  if (save_taps)
    set_output_complex_vector(PMT_EQU_TAPS, taps);
}

/**
 * takes a pointer to (padding_samp + code_samp + padding_samp) many complex
 * numbers and decodes the header using QPSK. The code length is calculating
 * from bytes_len.
 */
void packet_preamb_equalizer_impl::decode_header(const gr_complex *samples,
                                                 uint8_t *bytes,
                                                 int bytes_len) {
  int data_len = bytes_len * 8;
  int code_len = decoder.get_code_len(data_len);

  float symbols[code_len];
  fir.filterNdec((gr_complex *)symbols, samples, code_len / 2, samp_per_symb);

  assert(code_len % 11 != 0);
  float code_bits[code_len];
  for (int i = 0; i < code_len; i++)
    code_bits[(11 * i) % code_len] = symbols[i];

  uint8_t data_bits[data_len];
  decoder.decode(code_bits, data_len, data_bits);

  for (int i = 0; i < bytes_len; i++)
    bytes[i] = 0;

  for (int i = 0; i < data_len; i++)
    bytes[i / 8] |= (data_bits[i] & 0x1) << (i % 8);
}

} /* namespace marmote3 */
} // namespace gr
