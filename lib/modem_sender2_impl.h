/* -*- c++ -*- */
/*
 * Copyright 2017-2018 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_MODEM_SENDER2_IMPL_H
#define INCLUDED_MARMOTE3_MODEM_SENDER2_IMPL_H

#include <chrono>
#include <deque>
#include <gnuradio/thread/thread.h>
#include <marmote3/headers.h>
#include <marmote3/modem_sender2.h>
#include <pmt/pmt.h>
#include <random>
#include <unordered_map>
#include <vector>

namespace gr {
namespace marmote3 {

class modem_sender2_impl : public modem_sender2 {
protected:
  const int max_data_len;   // in bytes
  const int max_code_len;   // in bytes
  const int max_symb_len;   // in complex symbols
  const int max_queue_size; // in packets
  const pmt::pmt_t port_id;
  const int payload_mode; // how to calculate bytes statistics
  const std::vector<int> blocked_flowids;
  const int max_inflight_packets;
  const bool debug_scheduler;
  const bool debug_arq;

  bool enable_arq = false; // global ARQ enable flag

public:
  typedef std::chrono::system_clock::time_point time_point_t;
  typedef std::chrono::system_clock::duration duration_t;

  // round down to nearest whole second
  static time_point_t get_time_point_floor(time_point_t time);
  static double get_time_point_double(time_point_t time);

public:
  modem_sender2_impl(int max_data_len, int max_code_len, int max_symb_len,
                     int max_queue_size, int default_mcs,
                     float default_max_latency_s,
                     float default_max_throughput_bps,
                     bool default_is_file_mandate, int payload_mode,
                     int radio_id, const std::vector<int> &blocked_flowids,
                     int max_inflight_packets, bool debug_scheduler,
                     bool debug_arq);
  ~modem_sender2_impl() override;

  // ------- MUTEX -------

protected:
  gr::thread::mutex mutex; // protects all mutable data structures

  std::default_random_engine rand_engine;
  std::discrete_distribution<int> not_produced_mcs_boost{0.3, 0.5, 0.2};
  std::discrete_distribution<int> broadcast_mcs_boost{0.5, 0.5};
  std::uniform_int_distribution<int> rand_delay_gen{0, 4}; // multiplied by 5

  bool last_work_produced = true;
  long packet_id = 0; // incremented for each sent packet
  int radio_id;       // will be overwritten by actual message data

  // to avoid build up of packets between sender and subcarrier allocator
  int inflight_packets = 0;

public:
  int work(int noutput_items, gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items) override;

  void collect_block_report(ModemReport *modem_report) override;
  void process_block_command(const ModemCommand *modem_command) override;

  void process_received_arq_data(int source, int destination, int last_seqno,
                                 const std::vector<uint8_t> &report) override;
  void received_inflight_packet() override;

  // ------- MCS -------

protected:
  const int default_mcs;
  std::vector<int> mcs_assignment; // indexed by dst radio id

  int get_usable_mcs_index(int data_len, int index) const;
  void clear_mcs_assignment();

  // ------- MANDATE -------

protected:
  struct mandate_t {
    float max_latency_s;      // -1.0 if not specified
    float max_throughput_bps; // -1.0 if not specified
    bool is_file_mandate;
    unsigned int resend_count;

    mandate_t()
        : max_latency_s(0.0f), max_throughput_bps(0.0f), is_file_mandate(false),
          resend_count(0) {}
    mandate_t(float max_latency_s, float max_throughput_bps,
              bool is_file_mandate, unsigned int resend_count) {
      set(max_latency_s, max_throughput_bps, is_file_mandate, resend_count);
    }

    void set(float max_latency_s, float max_throughput_bps,
             bool is_file_mandate, unsigned int resend_count);
  };

  const mandate_t default_mandate;
  std::unordered_map<int, mandate_t> mandates; // indexed by flowid

  const mandate_t &get_mandate(int flowid) const;
  void set_mandate(int flowid, float max_latency_s, float max_throughput_bps,
                   bool is_file_mandate, unsigned int resend_count);

  // ------- PACKET -------

protected:
  enum state_t {
    STATE_WAITING = 0,
    STATE_SCHEDULED = 1, // must be sent to meet throughput requirement
    STATE_DROPPED = 2, // failed latency requirement, counted in packets_dropped
    STATE_SENT = 3,    // already sent, counted in packets_sent
  };

  struct packet_t {
    packet_t(time_point_t time, pmt::pmt_t data, int bytes_len)
        : time(time), data(data), bytes_len(bytes_len), arq_packet_marker(-1),
          mcs_packet_boost(0), sent_count(0) {}
    packet_t(const packet_t &packet) = delete;

    const time_point_t time; // time when enqueued
    const pmt::pmt_t data;
    const int bytes_len; // UDP/TCP payload length in bytes

    state_t state;
    int arq_packet_marker; // set when sent, reset when requeued
    int mcs_packet_boost;  // set when sent

    unsigned int sent_count; // how many times we have sent this packet
    time_point_t last_sent;  // valid if sent_count is not zero
  };

  // ------- LINK ARQ -------

  enum { ROUNDTRIP_HISTORY = 11 };

protected:
  // 0 -> 1 -> ... -> 255 -> 1 -> 2 -> ...
  static int arq_increment_seqno(int seqno);

  // -10000 if one is zero or (seqno1 - seqno2) mod 255 in [min, min+254]
  static int arq_seqno_difference(int seqno1, int seqno2, int min);

  int next_packet_marker = 0;

  struct arq_dst_t {
    arq_dst_t();

    struct sent_t {
      int seqno; // never zero in queue
      time_point_t time;
      int flowid;
      int marker;
      bool rescheduled;
      bool acknowledged; // with next_expected_seqno
    };

    duration_t roundtrips[ROUNDTRIP_HISTORY]; // in system clock
    unsigned int roundtrip_pos;
    duration_t median_roundtrip;
    int next_seqno = 0;      // next to be sent
    std::deque<sent_t> sent; // last pushed to the back
  };

  std::unordered_map<int, arq_dst_t> arq_dst_map; // sent arq data by dst

  // creates a new seqno, only last 8 bit is sent
  arq_dst_t::sent_t arq_create_sent_seqno(time_point_t now, int destination,
                                          int flowid);
  void arq_reschedule_missing(time_point_t now, int destination,
                              int missing_seqno);
  void arq_reschedule_recent(time_point_t now, int destination,
                             int next_expected_seqno);

  struct arq_src_t {
    struct rcvd_t {
      int seqno; // never zero in queue
      time_point_t time;
      bool missing;
      bool verified; // still missing after some time (not just reordered)
      int report_count;
    };

    int next_seqno = 0; // next to be rcvd
    int report_count = 0;
    std::deque<rcvd_t> rcvd; // last pushed to the back
  };

  std::unordered_map<int, arq_src_t> arq_src_map; // received messages

  void arq_process_rcvd_seqno(time_point_t now, int src, int received_seqno);
  void arq_collect_missing_seqnos(time_point_t now);
  std::vector<uint8_t> arq_generate_missing_report(int max_size);

  // ------- FLOW -------

protected:
  struct flow_t {
    int source; // key values, should never be changed
    int destination;
    int flowid;
    modem_sender2_impl *parent;

    mandate_t mandate;
    int max_bytes_sent;     // max payload to be sent per period in bytes
    duration_t max_latency; // max latency for packets to be sent

    unsigned long bytes_load = 0;     // enqueued payload bytes in report period
    unsigned long bytes_sent = 0;     // sent payload bytes in report period
    unsigned long bytes_dropped = 0;  // dropped payload bytes in report period
    unsigned long bytes_requeued = 0; // payload bytes of requeued messages
    float avg_bytes_load = 0.0f;      // moving average of bytes_load

    unsigned int packets_load = 0;     // number of received messages
    unsigned int packets_sent = 0;     // number of forwarded messages
    unsigned int packets_dropped = 0;  // number of dropped messages
    unsigned int packets_requeued = 0; // number of requeued messages

    int arq_flow_seqno = arq_header_t::initial_seqno;

    int mcs_flow_boost = 0;
    int mcs_boost_count = 0;

    std::deque<packet_t> packets;   // all remembered packets oredered in time
    bool dirty = false;             // schedule must be recalculated
    packet_t *selected = NULL;      // next packet to be sent
    unsigned long queued_bytes = 0; // sum of payloads in queue
    unsigned long unsent_bytes = 0; // WAITING or SCHEDULED and sent_count == 0
    float queued_max_wait = 0.0f;   // valid if selected != NULL

    std::deque<std::pair<time_point_t, int>> throttle; // for files
    int throttle_sent_bytes = 0;

    // delay to mitigate destination side collisions
    duration_t random_delay;

    flow_t(int source, int destination, int flowid, const mandate_t &mandate,
           modem_sender2_impl *parent);
    flow_t(const flow_t &) = delete;
    flow_t(flow_t &&) = default;
    flow_t &operator=(const flow_t &) = delete;
    flow_t &operator=(flow_t &&) = default;

    void set_mandate(const mandate_t &mandate);
    void add_packet(time_point_t time, pmt::pmt_t data, int payload_len);
    void mark_packet_dropped(std::deque<packet_t>::iterator pos);
    int get_selected_mcs_boost();
    void mark_selected_sent(int arq_packet_marker, time_point_t now);
    void mark_lost_waiting(time_point_t now, bool debug_arq,
                           int arq_packet_marker);
    void remove_front_packet();
    void remove_old_packets(time_point_t now);
    void schedule_next_packet(time_point_t now);
    int verify_period(std::deque<packet_t>::iterator &pos, time_point_t time1,
                      time_point_t time2, time_point_t now);
  };

  std::vector<flow_t> flows;
  flow_t *selected_flow = NULL; // updated by dequeue

  flow_t &find_flow(int source, int destination, int flowid);
  void remove_flow(size_t index); // replaces this with the last one
  bool is_second_more_important(time_point_t now, const flow_t &first,
                                const flow_t &second) const;
  bool is_second_more_important2(time_point_t now, const flow_t &first,
                                 const flow_t &second) const;

  void enqueue(time_point_t now, pmt::pmt_t msg); // add new message into queues
  pmt::pmt_t dequeue(time_point_t now); // select best message to be sent

public:
  static time_point_t get_mgen_timestamp(const uint8_t *data_ptr,
                                         size_t data_len, time_point_t now,
                                         int &seqno);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_MODEM_SENDER2_IMPL_H */
