/* -*- c++ -*- */
/*
 * Copyright 2017 Peter Horvath.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "fading_generator.h"

#include <functional>
#include <random>

#include <cassert>

namespace gr {
namespace marmote3 {

std::mt19937 mt_engine(2017);

extern std::mt19937 mt_engine;

fading_generator_correlated::fading_generator_correlated(
    double norm_doppler, DOPPLER_SPECTRUM spectrum)
    : norm_doppler(norm_doppler), doppler_spectrum(Jakes) {
        set_K_factor(0.0);
    }

fading_generator_correlated::~fading_generator_correlated() {}

void fading_generator_correlated::set_K_factor(double K_factor)  
{
  this->K_factor = K_factor;
  diffuse_power = sqrt(1.0/(1.0 + K_factor));
  los_power = sqrt(K_factor/(1.0 + K_factor));
}

void fading_generator_correlated::set_norm_doppler(double norm_doppler) {
  assert(norm_doppler > 0.0);
  this->norm_doppler = norm_doppler;
}

void fading_generator_correlated::set_doppler_spectrum(
    DOPPLER_SPECTRUM spectrum) {
  doppler_spectrum = spectrum;
}

fading_generator_sos::fading_generator_sos(double norm_doppler,
                                           DOPPLER_SPECTRUM spectrum)
    : fading_generator_correlated(norm_doppler, spectrum),
      nf(std::sqrt(2.0 / N)) {
  std::uniform_real_distribution<> un_2pi(-M_PI, M_PI);

  for (int ii = 0; ii < N; ++ii) {
    phases_i[ii] = un_2pi(mt_engine);
    phases_q[ii] = un_2pi(mt_engine);
  }

  for (int n = 0; n < N; ++n) {
    freqs_i[n] =
        2.0 * M_PI * norm_doppler * sin(M_PI * (2.0 * n + 1) / (4.0 * N));
    freqs_q[n] =
        2.0 * M_PI * norm_doppler * sin(M_PI * (2.0 * n + 1) / (4.0 * N));
  }

  /*for(auto n : freqs_i)  {
          cout << n << endl;
      }*/
}

fading_generator_sos::~fading_generator_sos() {}

void fading_generator_sos::generate(gr_complex *samples, int no_samples) {
  for (int ii = 0; ii < no_samples; ++ii) {
    float s_i = 0.0;
    float s_q = 0.0;
    for (int n = 0; n < N; ++n) {
      s_i += cos(phases_i[n]);
      s_q += cos(phases_q[n]);

      phases_i[n] += freqs_i[n];
      phases_q[n] += freqs_q[n];
    }

    samples[ii] = diffuse_power * gr_complex(s_i, s_q) + los_power;
  }
}

void fading_generator_sos::shift_time_offset(int no_samples) {
  for (int n = 0; n < N; ++n) {
    phases_i[n] += no_samples * freqs_i[n];
    phases_q[n] += no_samples * freqs_q[n];
  }
}

} // namespace marmote3
} // namespace gr
