import numpy as np
import matplotlib.pyplot as plt
import scipy.signal
import marmote3

txtaps = marmote3.get_fbmc_taps8(20, 22, "tx")
rxtaps = marmote3.get_fbmc_taps8(20, 22, "rx")
# eqtaps=marmote3.get_fbmc_taps8(20, 22, "eq")
eqtaps = np.array(
    [-0.03679217595526768, -0.10497455543232498, 0.11456993708646392, 0.062313102909099076, -0.18319592716835853, -0.05064906915962518, 0.4062625466235558,
     0.7340873960166299, 0.4062625466235558, -0.05064906915962518, -0.18319592716835853, 0.062313102909099076, 0.11456993708646392, -0.10497455543232498, -0.03679217595526768])

lo = len(eqtaps)
for ii in range(lo):
    eqtaps = np.insert(eqtaps, lo - ii - 1, np.zeros((10,)))

# print(eqtaps)

# w, h = scipy.signal.freqz(np.convolve(np.convolve(rxtaps, eqtaps), txtaps))
w, h = scipy.signal.freqz(np.convolve(rxtaps, eqtaps))
# w, h = scipy.signal.freqz(rxtaps)
f1 = np.array([.1, .2, .3, .4, .5, .6, .7, .77, .8, .85, .93, 1.8, 2.92])
p1 = np.array(
    [0, 0, -0.5, -2, -3.2, -9, -27, -47, -53, -58, -65, -65.5, -66.4])

# mask
f2 = np.array([0, 0, -40, -65, -65])
p2 = np.array([0, 0.625, 0.75, 1, 2.5])


plt.figure()
plt.plot(w[:100] / np.pi * 12.5, 20 * np.log10(abs(h[:100]))
         - 20 * np.log10(max(np.abs(h))), 'r--')
plt.plot([0.625, 0.625], [-100, 0], 'b-.')
plt.plot(p2, f2, 'k-')

plt.xlabel('Offset from the carrier [MHz]')
plt.ylabel('RX spectrum response [dBc]')
plt.grid()
plt.ylim([-80, 5])
plt.xlim([0, 2])
plt.show()
