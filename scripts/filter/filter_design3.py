#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from __future__ import print_function
import math
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
import torch
from torch.autograd import Function, Variable, gradcheck


class HalfFftFunction(Function):

    @staticmethod
    def forward(ctx, sig):
        sig = sig.numpy()
        result = np.concatenate((sig, sig[-1:0:-1]))
        result = np.fft.rfft(result, norm="ortho")
        # print("forward", sig, result.real)
        return torch.from_numpy(result.real)

    @staticmethod
    def backward(ctx, sig):
        sig = sig.data.numpy()
        result = np.concatenate((sig, sig[-1:0:-1]))
        result[0] *= 2.0
        result = np.fft.rfft(result, norm="ortho")
        result[0] *= 0.5
        # print("backward", sig, result.real)
        return Variable(torch.from_numpy(result.real))


half_fft = HalfFftFunction.apply
if False:
    sig = (Variable(torch.randn(5).double(), requires_grad=True),)
    print(gradcheck(half_fft, sig))


def half_to_full(sig):
    """Converts a half vector to a full symmetric vector."""
    indices = range(sig.size(0) - 1, 0, -1) + range(sig.size(0))
    return torch.index_select(sig, 0, Variable(torch.LongTensor(indices)))


def full_to_half(sig):
    """Converts a full (symmetric) vector to its half."""
    assert sig.size(0) % 2 == 1
    mid = (sig.size(0) + 1) / 2
    r1 = sig.narrow(0, mid - 1, mid)
    r2 = torch.index_select(
        sig, 0, Variable(torch.LongTensor(range(mid - 1, -1, -1))))
    return (r1 + r2) * 0.5

if False:
    var = Variable(torch.randn(5).double(), requires_grad=True)
    res = torch.sum(half_to_full(full_to_half(var)))
    # res = torch.sum(full_to_half(half_to_full(var)))
    res.backward()
    print(var.grad)


def full_firwin(numtaps, cutoff, window="hamming"):
    """Returns the half taps for the given bandpass filter."""
    assert numtaps % 2 == 1
    taps = signal.firwin(numtaps, cutoff, window=window)
    taps /= math.sqrt(np.sum(np.square(taps)))
    return Variable(torch.from_numpy(taps))


def square(sig):
    return sig * sig


def log10(sig):
    return (1.0 / math.log(10)) * torch.log(sig)


if False:
    taps = full_firwin(351, 0.1, window="flattop")
    plt.plot(taps.data.numpy())
    spec = half_to_full(half_fft(full_to_half(taps)))
    plt.plot(spec.data.numpy())
    plt.show()
    plt.plot(10.0 * log10(square(spec)).data.numpy())
    plt.show()


def full_papr_db(taps, symb_sep):
    assert taps.size(0) % 2 == 1 and symb_sep > 0
    mid = ((taps.size(0) - 1) / 2) % symb_sep
    if taps.size(0) % symb_sep != 0:
        size = [symb_sep - (taps.size(0) % symb_sep)] + list(taps.size()[1:])
        taps = torch.cat([taps, Variable(torch.zeros(tuple(size)).double())])
    size = [taps.size(0) / symb_sep, symb_sep] + list(taps.size()[1:])
    peak = torch.sum(torch.abs(taps.view(size)), dim=0)
    peak = 20.0 * log10(peak * math.sqrt(symb_sep))
    if mid > 0:
        indices = range(mid, symb_sep) + range(mid)
        peak = torch.index_select(peak, 0, Variable(torch.LongTensor(indices)))
    return peak - 10.0 * log10(torch.sum(square(taps)))


if False:
    sig = full_firwin(351, 0.05, window="flattop")
    plt.plot(full_papr_db(sig, 22).data.numpy())
    plt.show()


def half_supress_db(taps, passband, stopband):
    assert 0.0 < passband and passband < stopband and stopband < 0.5
    passband = int(passband * (2 * taps.size(0) + 1))
    stopband = int(stopband * (2 * taps.size(0) + 1)) + 1
    power = square(half_fft(taps))
    a = torch.min(power.narrow(0, 0, passband))
    b = torch.max(power.narrow(0, stopband, power.size(0) - stopband))
    return 10.0 * log10(b / a)

if False:
    sig = full_to_half(full_firwin(101, 0.10, window="flattop"))
    print(half_supress_db(sig, 0.05, 0.15))
    plt.plot(10.0 * log10(square(half_fft(sig))).data.numpy())
    plt.show()

if True:
    init = full_to_half(full_firwin(251, 1.0 / 20, window="flattop"))
    # init = Variable(torch.randn(init.data.size(0)).double())
    taps = Variable(init.data, requires_grad=True)
    opti = torch.optim.Adam([taps], lr=1e-10)

    for step in range(300001):
        # print(torch.sum(taps.data * taps.data))
        opti.zero_grad()
        papr = torch.max(full_papr_db(half_to_full(taps), 22))
        supr = half_supress_db(taps, 0.95 / 20, 1.05 / 20)
        loss = papr.clamp(min=3) + supr.clamp(min=-80)
        loss.backward()
        opti.step()

        if step % 1000 == 0:
            print(step, float(loss.data), float(papr), float(supr))

        if step % 10000 == 0:
            plt.plot(half_to_full(taps).data.numpy())
            plt.plot(half_to_full(half_fft(taps)).data.numpy())
            plt.show()
