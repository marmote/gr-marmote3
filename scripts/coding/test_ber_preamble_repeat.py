#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Test Ber Preamble Repeat
# Generated: Tue Jul  2 01:08:05 2019
##################################################

from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import marmote3
import math
import numpy as np


class test_ber_preamble_repeat(gr.top_block):

    def __init__(self, data_len=1500, mod_index=1, num_packets=1000, repeat=2, snr=12):
        gr.top_block.__init__(self, "Test Ber Preamble Repeat")

        ##################################################
        # Parameters
        ##################################################
        self.data_len = data_len
        self.mod_index = mod_index
        self.num_packets = num_packets
        self.repeat = repeat
        self.snr = snr

        ##################################################
        # Variables
        ##################################################
        self.payload_len = payload_len = 8 * data_len * repeat / mod_index
        self.padding_len = padding_len = 6
        self.chirp_len = chirp_len = 128
        self.taps = taps = firdes.low_pass(2.0, 2.0, 0.5, 0.2)
        self.packet_len = packet_len = padding_len + chirp_len + padding_len + payload_len
        self.chirp = chirp = marmote3.get_chirp_taps(chirp_len)

        ##################################################
        # Blocks
        ##################################################
        self.marmote3_packet_symb_mod_0 = marmote3.packet_symb_mod(None or mod_index, payload_len)
        self.marmote3_packet_symb_demod_0 = marmote3.packet_symb_demod(None or mod_index, 24, data_len * 8 * repeat)
        self.marmote3_packet_rep_encoder_0 = marmote3.packet_rep_encoder(data_len * 8,repeat)
        self.marmote3_packet_rep_decoder_0 = marmote3.packet_rep_decoder(data_len,repeat)
        self.marmote3_packet_preamb_insert_0 = marmote3.packet_preamb_insert((np.concatenate([chirp[-padding_len:],chirp,chirp[:padding_len]])), False, 0.0, marmote3.HDR_NONE, False, packet_len)
        self.marmote3_packet_preamb_equalizer_0_0 = marmote3.packet_preamb_equalizer((marmote3.circular_roll(marmote3.circular_conv(marmote3.interpolate(chirp, 2), taps), -(len(taps)-1)/2)), 2, padding_len, marmote3.HDR_NONE, False, payload_len, 0.2, False, False)
        self.marmote3_packet_preamb_equalizer_0_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_packet_freq_corrector_0_0 = marmote3.packet_freq_corrector(None or mod_index, 64, False, payload_len)
        self.marmote3_modem_bert_sender_0 = marmote3.modem_bert_sender(1, num_packets + 1, data_len)
        self.marmote3_modem_bert_receiver_0 = marmote3.modem_bert_receiver(1)
        self.interp_fir_filter_xxx_0 = filter.interp_fir_filter_ccc(2, (taps))
        self.interp_fir_filter_xxx_0.declare_sample_delay(0)
        self.blocks_tag_gate_0 = blocks.tag_gate(gr.sizeof_gr_complex * 1, False)
        self.blocks_tag_gate_0.set_single_key("")
        self.blocks_stream_to_tagged_stream_0 = blocks.stream_to_tagged_stream(gr.sizeof_gr_complex, 1, (padding_len + packet_len) * 2, "packet_len")
        (self.blocks_stream_to_tagged_stream_0).set_min_output_buffer(1048576)
        self.blocks_stream_mux_0 = blocks.stream_mux(gr.sizeof_gr_complex*1, (padding_len, packet_len))
        self.blocks_skiphead_2 = blocks.skiphead(gr.sizeof_gr_complex*1, (len(taps)-1)/2 + 2 * padding_len)
        self.blocks_multiply_xx_0 = blocks.multiply_vcc(1)
        self.blocks_add_xx_0 = blocks.add_vcc(1)
        self.analog_sig_source_x_0 = analog.sig_source_c(1, analog.GR_COS_WAVE, 0.000001, 1.0, 0)
        self.analog_noise_source_x_0 = analog.noise_source_c(analog.GR_GAUSSIAN, 1.0 / math.sqrt(10**(float(snr) / 10.0)), 0)
        self.analog_const_source_x_0 = analog.sig_source_c(0, analog.GR_CONST_WAVE, 0, 0, 0)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_const_source_x_0, 0), (self.blocks_stream_mux_0, 0))
        self.connect((self.analog_noise_source_x_0, 0), (self.blocks_add_xx_0, 1))
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_multiply_xx_0, 1))
        self.connect((self.blocks_add_xx_0, 0), (self.blocks_multiply_xx_0, 0))
        self.connect((self.blocks_multiply_xx_0, 0), (self.blocks_skiphead_2, 0))
        self.connect((self.blocks_skiphead_2, 0), (self.blocks_stream_to_tagged_stream_0, 0))
        self.connect((self.blocks_stream_mux_0, 0), (self.blocks_tag_gate_0, 0))
        self.connect((self.blocks_stream_to_tagged_stream_0, 0), (self.marmote3_packet_preamb_equalizer_0_0, 0))
        self.connect((self.blocks_tag_gate_0, 0), (self.interp_fir_filter_xxx_0, 0))
        self.connect((self.interp_fir_filter_xxx_0, 0), (self.blocks_add_xx_0, 0))
        self.connect((self.marmote3_modem_bert_sender_0, 0), (self.marmote3_packet_rep_encoder_0, 0))
        self.connect((self.marmote3_packet_freq_corrector_0_0, 0), (self.marmote3_packet_symb_demod_0, 0))
        self.connect((self.marmote3_packet_preamb_equalizer_0_0, 0), (self.marmote3_packet_freq_corrector_0_0, 0))
        self.connect((self.marmote3_packet_preamb_insert_0, 0), (self.blocks_stream_mux_0, 1))
        self.connect((self.marmote3_packet_rep_decoder_0, 0), (self.marmote3_modem_bert_receiver_0, 0))
        self.connect((self.marmote3_packet_rep_encoder_0, 0), (self.marmote3_packet_symb_mod_0, 0))
        self.connect((self.marmote3_packet_symb_demod_0, 0), (self.marmote3_packet_rep_decoder_0, 0))
        self.connect((self.marmote3_packet_symb_mod_0, 0), (self.marmote3_packet_preamb_insert_0, 0))

    def get_data_len(self):
        return self.data_len

    def set_data_len(self, data_len):
        self.data_len = data_len
        self.set_payload_len(8 * self.data_len * self.repeat / self.mod_index)

    def get_mod_index(self):
        return self.mod_index

    def set_mod_index(self, mod_index):
        self.mod_index = mod_index
        self.set_payload_len(8 * self.data_len * self.repeat / self.mod_index)

    def get_num_packets(self):
        return self.num_packets

    def set_num_packets(self, num_packets):
        self.num_packets = num_packets

    def get_repeat(self):
        return self.repeat

    def set_repeat(self, repeat):
        self.repeat = repeat
        self.set_payload_len(8 * self.data_len * self.repeat / self.mod_index)

    def get_snr(self):
        return self.snr

    def set_snr(self, snr):
        self.snr = snr
        self.analog_noise_source_x_0.set_amplitude(1.0 / math.sqrt(10**(float(self.snr) / 10.0)))

    def get_payload_len(self):
        return self.payload_len

    def set_payload_len(self, payload_len):
        self.payload_len = payload_len
        self.set_packet_len(self.padding_len + self.chirp_len + self.padding_len + self.payload_len)

    def get_padding_len(self):
        return self.padding_len

    def set_padding_len(self, padding_len):
        self.padding_len = padding_len
        self.set_packet_len(self.padding_len + self.chirp_len + self.padding_len + self.payload_len)
        self.blocks_stream_to_tagged_stream_0.set_packet_len((self.padding_len + self.packet_len) * 2)
        self.blocks_stream_to_tagged_stream_0.set_packet_len_pmt((self.padding_len + self.packet_len) * 2)

    def get_chirp_len(self):
        return self.chirp_len

    def set_chirp_len(self, chirp_len):
        self.chirp_len = chirp_len
        self.set_packet_len(self.padding_len + self.chirp_len + self.padding_len + self.payload_len)
        self.set_chirp(marmote3.get_chirp_taps(self.chirp_len))

    def get_taps(self):
        return self.taps

    def set_taps(self, taps):
        self.taps = taps
        self.interp_fir_filter_xxx_0.set_taps((self.taps))

    def get_packet_len(self):
        return self.packet_len

    def set_packet_len(self, packet_len):
        self.packet_len = packet_len
        self.blocks_stream_to_tagged_stream_0.set_packet_len((self.padding_len + self.packet_len) * 2)
        self.blocks_stream_to_tagged_stream_0.set_packet_len_pmt((self.padding_len + self.packet_len) * 2)

    def get_chirp(self):
        return self.chirp

    def set_chirp(self, chirp):
        self.chirp = chirp


def argument_parser():
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option)
    parser.add_option(
        "-d", "--data-len", dest="data_len", type="intx", default=1500,
        help="Set data length [default=%default]")
    parser.add_option(
        "-m", "--mod-index", dest="mod_index", type="intx", default=1,
        help="Set modulation order [default=%default]")
    parser.add_option(
        "-p", "--num-packets", dest="num_packets", type="intx", default=1000,
        help="Set number of packets [default=%default]")
    parser.add_option(
        "", "--repeat", dest="repeat", type="intx", default=2,
        help="Set repeat [default=%default]")
    parser.add_option(
        "-s", "--snr", dest="snr", type="eng_float", default=eng_notation.num_to_str(12),
        help="Set SNR [default=%default]")
    return parser


def main(top_block_cls=test_ber_preamble_repeat, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    tb = top_block_cls(data_len=options.data_len, mod_index=options.mod_index, num_packets=options.num_packets, repeat=options.repeat, snr=options.snr)
    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
