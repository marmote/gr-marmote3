#!/usr/bin/env bash

# Provision an LXC container with MarmotE modem based radio

# This script launches an LXC container, copies itself into the container
# and runs itself again to install the gr-marmote3 modem and its dependendies.
# Finally it creates an additional network interface required by the radio.

# Copyright 2019 Sandor Szilvasi


if [ -z "${1##*[!0-9]*}" ] ; then
   echo "Usage: $0 <RADIO_ID>"
   exit 1
fi

RADIO_ID_DEC=$1
RADIO_ID_HEX=$(printf "%02X" ${RADIO_ID_DEC})
CONTAINER="radio${RADIO_ID_DEC}"

if ! sudo grep -qa 'container=lxc' /proc/1/environ; then

    echo "Executing on host machine '$(hostname)'"

    # Create container
    lxc launch ubuntu:18.04 ${CONTAINER}

    # Copy this script to container
    lxc file push "$0" ${CONTAINER}/tmp/

    # Execute this script in container
    lxc exec ${CONTAINER} "/tmp/$0" "${RADIO_ID_DEC}"

    # Restart container
    # lxc restart ${CONTAINER}  # currently removes tap0 interface

else

    echo "Executing inside container '${CONTAINER}'"

    sudo apt-get update

    # Install basic dependencies
    sudo apt-get install -y git cmake g++ python3-pip libusb-1.0-0-dev

    # Install UHD and GNU Radio
    sudo apt-get install -y libuhd-dev gnuradio-dev
    sudo /usr/lib/uhd/utils/uhd_images_downloader.py

    # Install gr-marmote3 dependencies
    sudo apt-get install -y libprotobuf-dev libzmq3-dev swig3.0 protobuf-compiler python-protobuf

    # Build gr-marmote3
    git clone https://gitlab.com/marmote/gr-marmote3.git
    cd gr-marmote3 && mkdir -p build && cd build && cmake ..
    make -j && make test && sudo make install && sudo ldconfig

    # Create network interface for radio
    ip tuntap add dev tap0 mode tap
    sudo ip link set dev tap0 mtu 1500 address 12:34:56:78:90:${RADIO_ID_HEX} up
    sudo ip addr flush dev tap0
    sudo ip addr add 192.168.45.${RADIO_ID_DEC}/24 dev tap0
    sudo ip neigh replace 192.168.45.${RADIO_ID_DEC} lladdr 12:34:56:78:90:${RADIO_ID_HEX} nud permanent dev tap0

fi
