# Old TODO list for the DARPA SC2 challenge

## TODO after PE2 (if we continue)

* Handle safe unregistering of monitored objects (see modem_monitor_impl.cc)
* Fix the even byte length limitation of the RA en/decoder.
* More the CRC calculation/verification into a separate block
* Move the ARQ functionality into a separate block after modem sender
* Combine blocks under a single thread (block/scheduler) to improve latency
* Use offset modulation (OBPSK, OQPSK, O16QAM, O64QAM) for improved PAPR
* Move synchronization to the end of packet to improve latency
* Better ARQ and FLOW logging / debugging

## TODO OLD

### For PE1 redux

* Redesign filters for better PAPR
* Leave gap between packets for better SNR measurement 
  (if needed), and report gap SNR and PAPR to the MAC
* Use receiver side channel SNR and PAPR values for 
  transmittert side channel selection
* Move preamble to the end of the packet (improves latency)
* Change header coding from 1/3 to 1/4 convolutional code
* Better equalizer (maybe filter out the noise from preamble)
* Use offset rotator from marmote2, use MCS config
* Use historic data for MAC channel decision
* Report channel usage on collab interface
* Use collab data for MAC channel decision
* MCS calculation override for abviously bad index
* Ask PAUL what to implement for PE1

### Important

* TX gaion control now based on out of band spurs, but that
  is also triggered by opponent clipping, so redesign the
  detection based on preamble (put a large spike there)
* ARQ or HARQ (emergizer) error control feedback
* Develop anti clipping filter
* Parse/store data into SQLite database
* Identify spectrograms (which team, what modulation, etc)
* Rewrite the inner sender streamed blocks for smaller latency
* Calculate the priority from packet numbers or bytes per flow

### Backburner

* Think about routing (decentralized maybe)
* Transfer some blocks to FPGA
* Finish the better modem sender queue with better prioritization
* Properly permute the bits in the RA code
* Allow odd number of bytes for RA coder/decoder
* A strong carrier will throw off the freq estimator, do something

## RF design

### General issues

* Try to do it in software first, then move it to RFNoC via HLS if bandwidth and response time becomes and issue (try to refactor and share C++ code between GNURadio and HLS).
* Use the full spectrum on the RX side and do digital processing to separate useful content. This will give RF situational awareness, but strong transmissions (even ours) can completely saturate the digital channel.
* Use FBMC to separate channels, already implemented fractional synthetiser and channelizer, this is a perfectly viable and very flexible option, allows multiple users on adjacent channels (to others we can appear as a single wide channel).
* With OFDM we cannot put two users next to each other so we have to leave gaps, which pushes us to use more channels. I do not think that more than 128 channels is going to be realistic within the 100 MHz bandwidth, and that might not be possible to implement b/c of phase noise. PAPR is also an issue with OFDM, so the pragmatic choice is FBMC with a relatively few channels (maybe 16 or 32).
* Probably we should target very efficient transmissions meaning that we should try to support 16/64 QAM symbols. This means that most inter symbol/channel interference must be eliminated (this is why we need the fractional FBMC synthesizer).
* Eventually we want to use 2x2 MIMO, so our synchronization algorithms must allow that. Probably we have to support a few sample delays between the two streams (b/c of software/USRP/simulator/channel delays).
* 2x2 MIMO means that we cannot decode even one of the two streams without knowing perfectly all four channels between the four antennae.

### Synchronization

* We could use inter symbol spacing (periodic energy at the edges of the channel bandwidth) to detect our own signal, but then MIMO transmissions will overlap and would 1) prevent the use of the same spacing for the two antennae, 2) result in different payload capability. This means that even for bit synchronization we cannot use this trick (which made the SC1 competitive radio good).
* Because of MIMO we are going to operate at around 0dB SNR level (two overlapping transmissions are received), so data independent synchronization is  going to be challenging. In particular, small frequency error correction should probably be done data aided, or do it after the data streams are separated. The real question is if small frequency error is going to be the same for the two streams (if the simulator is right, then it should).
* Synchronization data should be sent orthogonally (either in time, frequency, code) on the two MIMO channels. Frequency and short code orthogonality might not work well, because we have two different channels and will not be able to differentiate the two channels. If we separate in time, then one stream transmits the training data while the other sends zero (otherwise we are back in the 0 dB SNR case).

### Equalization

* We can mimic the benefit of cyclic prefix with periodic training data by subtracting the training data so we have cyclic (zero data instead of the prefix) data and equalize in the frequency domain. The other option is to equalize in the time domain, but then we need to keep our data oversampled to limit the length of our filter.
* Probably because of MIMO we should equalize everything in a single go, set up the transformation via SVD, so equalization should not be limited to a single received stream. This means that the channel has to be measured in a single go (cannot separate bit synchronization, single stream equalization). Probably we can leave the small frequency error after the equalization (except if the whole system is not phase coherent!).
* If we separate the training data in time, then we can completely measure the two channels, and do multiple SVD for different parts of the message.

### Questions

* Is the simulator/USRP going to be phase coherent and not introduce different small frequency errors on separate channels for 2x2 MIMO?
* Gain control is a huge problem, within the sample scenarios we see 50 dB path loss (10m apart) vs 95 dB path loss (1km apart). Can we actually get all of the 100 MHz bandwidth to the digital domain and do processing there or we have to frequency hop physically?