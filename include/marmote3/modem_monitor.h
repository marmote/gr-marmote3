/* -*- c++ -*- */
/*
 * Copyright 2018 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_MODEM_MONITOR_H
#define INCLUDED_MARMOTE3_MODEM_MONITOR_H

#include <gnuradio/block.h>
#include <marmote3/api.h>

namespace gr {
namespace marmote3 {

class MARMOTE3_API modem_monitored;
class ModemReport;
class ModemCommand;

/*!
 * \brief <+description of block+>
 * \ingroup marmote3
 */
class MARMOTE3_API modem_monitor : virtual public gr::block {
public:
  typedef boost::shared_ptr<modem_monitor> sptr;

  /*!
   * \brief Return a shared_ptr to a new instance of marmote3::modem_monitor.
   *
   * To avoid accidental use of raw pointers, marmote3::modem_monitor's
   * constructor is in a private implementation
   * class. marmote3::modem_monitor::make is the public interface for
   * creating new instances.
   */
  static sptr make(int report_rate, const std::string &report_addr,
                   int print_mode, const std::string &command_addr);

#ifndef SWIG
  virtual void register_monitored(modem_monitored *monitored) = 0;
  virtual void unregister_monitored(modem_monitored *monitored) = 0;
#endif

  virtual std::string collect_serialized_report() = 0; // for testing only
  virtual void process_serialized_command(
      const std::string &command) = 0; // for testing only
};

class MARMOTE3_API modem_monitored {
private:
  struct min_max_t {
    int min = std::numeric_limits<int>::max();
    int max = std::numeric_limits<int>::min();

    inline void update(int num) {
      min = std::min(min, num);
      max = std::max(max, num);
    }

    inline void reset() {
      min = std::numeric_limits<int>::max();
      max = std::numeric_limits<int>::min();
    }
  };

  int work_counter = 0;
  std::vector<struct min_max_t> input_items;
  std::vector<struct min_max_t> output_space;
  gr::thread::mutex mutex;

protected:
  modem_monitor::sptr monitor; // to keep the monitor alive
  gr::thread::mutex monitor_mutex;

  virtual ~modem_monitored() { unregister_monitor(); }

public:
  void register_monitor(const modem_monitor::sptr &monitor);
  void unregister_monitor();

  virtual gr::block *get_this_block();
  virtual std::string get_alias();

  void monitor_work(); // called from work

#ifndef SWIG
  virtual void collect_block_report(ModemReport *modem_report);
  void collect_perf_report(ModemReport *modem_report);
  virtual void process_block_command(const ModemCommand *modem_command);
#endif
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_MODEM_MONITOR_H */
