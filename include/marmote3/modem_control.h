/* -*- c++ -*- */
/*
 * Copyright 2018 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_MODEM_CONTROL_H
#define INCLUDED_MARMOTE3_MODEM_CONTROL_H

#include <gnuradio/feval.h>
#include <gnuradio/thread/thread.h>
#include <marmote3/api.h>
#include <marmote3/modem_monitor.h>

namespace gr {
namespace marmote3 {

/*!
 * \brief <+description+>
 */
class MARMOTE3_API modem_control : public modem_monitored {
protected:
  const std::string center_freq_var;
  const std::string samp_rate_var;
  const std::string rx_gain_var;
  const std::string tx_gain_var;
  const std::string tx_power_var;
  const std::string invert_spectrum_var;
  const std::string enable_recording_var;

  gr::thread::mutex mutex;

  gr::feval_p *modem_setter = NULL;
  double center_freq = 0.0f;
  double samp_rate = 0;
  float rx_gain = 0.0f;
  float tx_gain = 0.0f;
  float tx_power = 0.0f;
  bool invert_spectrum = false;
  bool enable_recording = false;
  int num_channels = 0;
  int num_subcarriers = 0;

  float nominal_rx_gain = -1.0f;
  float nominal_tx_gain = -1.0f;

public:
  modem_control(const std::string &center_freq_var,
                const std::string &samp_rate_var,
                const std::string &rx_gain_var, const std::string &tx_gain_var,
                const std::string &tx_power_var,
                const std::string &invert_spectrum_var,
                const std::string &enable_recording_var)
      : center_freq_var(center_freq_var), samp_rate_var(samp_rate_var),
        rx_gain_var(rx_gain_var), tx_gain_var(tx_gain_var),
        tx_power_var(tx_power_var), invert_spectrum_var(invert_spectrum_var),
        enable_recording_var(enable_recording_var) {}

  std::string get_alias() override;

  // WARNING: You must keep the modem_setter alive in Python
  void set_modem_setter(gr::feval_p *modem_setter);

  // Update values from python (called indirectly through block command)
  void set_center_freq(double hz = 0.0f);
  void set_samp_rate(double hz = 0);
  void set_rx_gain(float db = 0.0f);
  void set_tx_gain(float db = 0.0f);
  void set_tx_power(float db = 0.0f);
  void set_invert_spectrum(int flag = 0);
  void set_enable_recording(int flag = 0);
  void set_num_channels(int num = 0);
  void set_num_subcarriers(int num = 0);

#ifndef SWIG
  void collect_block_report(ModemReport *modem_report) override;
  void process_block_command(const ModemCommand *modem_command) override;
#endif

  long find_good_samp_rate(double rf_bandwidth);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_MODEM_CONTROL_H */
