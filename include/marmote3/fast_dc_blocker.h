/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_FAST_DC_BLOCKER_H
#define INCLUDED_MARMOTE3_FAST_DC_BLOCKER_H

#include <gnuradio/sync_block.h>
#include <marmote3/api.h>

namespace gr {
namespace marmote3 {

/*!
 * \brief <+description of block+>
 * \ingroup marmote3
 */
class MARMOTE3_API fast_dc_blocker : virtual public gr::sync_block {
public:
  typedef boost::shared_ptr<fast_dc_blocker> sptr;

  /*!
   * \brief Return a shared_ptr to a new instance of marmote3::fast_dc_blocker.
   *
   * To avoid accidental use of raw pointers, marmote3::fast_dc_blocker's
   * constructor is in a private implementation
   * class. marmote3::fast_dc_blocker::make is the public interface for
   * creating new instances.
   */
  static sptr make(int length);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_FAST_DC_BLOCKER_H */
