#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3
import numpy as np


class qa_packet_ampl_corrector (gr_unittest.TestCase):

    def test0(self):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_c([1, 1j, -2, -2j], False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_gr_complex, 1, 4, "packet_len"),
            marmote3.packet_ampl_corrector(2, 4),
            sink
        )
        top.run()
        print(sink.data())
        self.assertTrue(
            np.allclose(sink.data(), [1, 1j,  -1, -1j]))


if __name__ == '__main__':
    gr_unittest.run(qa_packet_ampl_corrector)
