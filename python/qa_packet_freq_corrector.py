#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3
import numpy as np


class qa_packet_freq_corrector (gr_unittest.TestCase):

    def test0(self):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_c([1 + 1j, -1 + 1j, -1 - 1j, 1 - 1j], False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_gr_complex, 1, 4, "packet_len"),
            marmote3.packet_freq_corrector(marmote3.MOD_QPSK, 1, False, 4),
            sink
        )
        top.run()
        print(sink.data())
        self.assertTrue(
            np.allclose(sink.data(), [1 + 1j, -1 + 1j, -1 - 1j, 1 - 1j]))

    def test1(self):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_c(np.exp(0.3j + 0.1j * np.arange(4)), False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_gr_complex, 1, 4, "packet_len"),
            marmote3.packet_freq_corrector(marmote3.MOD_QPSK, 1, False, 4),
            sink
        )
        top.run()
        print(sink.data())
        self.assertTrue(
            np.allclose(sink.data(), np.ones(4) * np.exp(0.25j * np.pi)))

    def test2(self):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_c(np.exp(0.3j + 0.1j * np.arange(4)), False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_gr_complex, 1, 4, "packet_len"),
            marmote3.packet_freq_corrector(marmote3.MOD_QPSK, 2, False, 4),
            sink
        )
        top.run()
        print(sink.data())
        self.assertTrue(
            np.allclose(sink.data(), np.ones(4) * np.exp(0.25j * np.pi)))

    def test3(self):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_c(
                np.exp(np.pi * (0.7j + 0.01j * np.arange(100))), False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_gr_complex, 1, 100, "packet_len"),
            marmote3.packet_freq_corrector(marmote3.MOD_QPSK, 1, False, 100),
            sink
        )
        top.run()
        print(sink.data())
        print(np.ones(100) * np.exp(0.75j * np.pi))
        self.assertTrue(
            np.allclose(sink.data(), np.ones(100) * np.exp(0.75j * np.pi)))

    def test4(self):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_c(
                np.exp(np.pi * (1.2j + 0.01j * np.arange(100))), False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_gr_complex, 1, 100, "packet_len"),
            marmote3.packet_freq_corrector(marmote3.MOD_QPSK, 10, False, 100),
            sink
        )
        top.run()
        print(sink.data())
        self.assertTrue(
            np.allclose(sink.data(), np.ones(100) * np.exp(1.25j * np.pi)))


if __name__ == '__main__':
    gr_unittest.run(qa_packet_freq_corrector)
