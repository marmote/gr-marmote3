#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

import time
from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3
import pmt


class qa_message_to_packet (gr_unittest.TestCase):

    def test1(self):
        top = gr.top_block()
        source = marmote3.message_to_packet(1, 100, 1)
        sink = blocks.vector_sink_b()
        top.connect(source, sink)
        top.start()

        port = pmt.intern("in")
        data = pmt.make_u8vector(16, 0)
        for i in range(16):
            pmt.u8vector_set(data, i, i)
        msg = pmt.cons(pmt.PMT_NIL, data)
        source.to_basic_block()._post(port, msg)

        while len(sink.data()) == 0:
            time.sleep(0.05)
        top.stop()
        top.wait()
        print(sink.data())
        self.assertTrue(list(sink.data()) == list(range(16)))


if __name__ == '__main__':
    gr_unittest.run(qa_message_to_packet)
