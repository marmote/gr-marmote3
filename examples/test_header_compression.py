#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Test Header Compression
# GNU Radio version: 3.8.2.0

from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import marmote3


class test_header_compression(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Test Header Compression")

        ##################################################
        # Variables
        ##################################################
        self.modem_monitor = marmote3.modem_monitor(1, "tcp://127.0.0.1:7557", 1, "tcp://127.0.0.1:7555")

        ##################################################
        # Blocks
        ##################################################
        self.msg_connect((self.modem_monitor, 'unused'), (self.modem_monitor, 'unused'))
        self.marmote3_test_message_source_0 = marmote3.test_message_source(8, 1000, 3, 100000, 200, 0, 101, 102)
        self.marmote3_test_message_source_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_test_message_sink_0 = marmote3.test_message_sink(3)
        self.marmote3_test_message_sink_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_header_decompressor_0 = marmote3.header_decompressor(3, True)
        self.marmote3_header_decompressor_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_header_compressor_0 = marmote3.header_compressor(3, True)
        self.marmote3_header_compressor_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.marmote3_header_compressor_0, 'out'), (self.marmote3_header_decompressor_0, 'in'))
        self.msg_connect((self.marmote3_header_decompressor_0, 'out'), (self.marmote3_test_message_sink_0, 'in'))
        self.msg_connect((self.marmote3_test_message_source_0, 'out'), (self.marmote3_header_compressor_0, 'in'))


    def get_variable_marmote3_modem_monitor_0(self):
        return self.variable_marmote3_modem_monitor_0

    def set_variable_marmote3_modem_monitor_0(self, variable_marmote3_modem_monitor_0):
        self.variable_marmote3_modem_monitor_0 = variable_marmote3_modem_monitor_0





def main(top_block_cls=test_header_compression, options=None):
    tb = top_block_cls()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    try:
        input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
