#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Streaming EVM
# GNU Radio version: 3.8.5.0

from distutils.version import StrictVersion

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

from PyQt5 import Qt
from gnuradio import qtgui
from gnuradio.filter import firdes
import sip
from gnuradio import blocks
import numpy
from gnuradio import digital
from gnuradio import fft
from gnuradio.fft import window
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import marmote3
import numpy as np

from gnuradio import qtgui

class test_streaming_evm(gr.top_block, Qt.QWidget):

    def __init__(self, num_channels=16):
        gr.top_block.__init__(self, "Streaming EVM")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Streaming EVM")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "test_streaming_evm")

        try:
            if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
                self.restoreGeometry(self.settings.value("geometry").toByteArray())
            else:
                self.restoreGeometry(self.settings.value("geometry"))
        except:
            pass

        ##################################################
        # Parameters
        ##################################################
        self.num_channels = num_channels

        ##################################################
        # Variables
        ##################################################
        self.fbmc_tx_stride = fbmc_tx_stride = {16: 22, 20: 28, 24: 34, 32: 46, 40: 58}[num_channels]
        self.fbmc_fft_len = fbmc_fft_len = int(num_channels * 5 // 4)
        self.fbmc_eq_taps = fbmc_eq_taps = marmote3.get_fbmc_taps8(fbmc_fft_len, fbmc_tx_stride, "eq")
        self.subcarrier_map = subcarrier_map = [0,1,2,3,4]
        self.fbmc_rx_taps = fbmc_rx_taps = marmote3.get_fbmc_taps8(fbmc_fft_len, fbmc_tx_stride, "rx")
        self.fbmc_eq2_taps = fbmc_eq2_taps = [fbmc_eq_taps[i // 2] if i % 2 == 0 else 0.0 for i in range(2*len(fbmc_eq_taps)-1)]
        self.xmod = xmod = digital.constellation_16qam()
        self.subcarrier_inv = subcarrier_inv = [subcarrier_map.index(x) if x in subcarrier_map else -1 for x in range(fbmc_fft_len)]
        self.samp_rate = samp_rate = 20e6
        self.fbmc_tx_taps = fbmc_tx_taps = marmote3.get_fbmc_taps8(fbmc_fft_len, fbmc_tx_stride, "tx")
        self.fbmc_rxeq_taps = fbmc_rxeq_taps = np.convolve(fbmc_rx_taps, fbmc_eq2_taps, mode="full")
        self.fbmc_rx_stride = fbmc_rx_stride = fbmc_tx_stride

        ##################################################
        # Blocks
        ##################################################
        self.synt_subcarrier_remapper = marmote3.subcarrier_remapper(len(subcarrier_map), subcarrier_inv, 0)
        self.synt_polyphase_synt_filter = marmote3.polyphase_synt_filter_ccf(fbmc_fft_len, fbmc_tx_taps, fbmc_tx_stride or fbmc_fft_len)
        self.synt_polyphase_synt_filter.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.synt_polyphase_rotator = marmote3.polyphase_rotator_vcc(fbmc_fft_len, subcarrier_map, fbmc_tx_stride)
        self.synt_periodic_multiply_const = marmote3.periodic_multiply_const_cc(1, np.exp(1.0j * np.pi / fbmc_fft_len * np.arange(0, 2 * fbmc_fft_len)))
        self.synt_fft = fft.fft_vcc(fbmc_fft_len, False, window.rectangular(fbmc_fft_len), False, 1)
        self.qtgui_freq_sink_x_0 = qtgui.freq_sink_c(
            1024, #size
            firdes.WIN_BLACKMAN_hARRIS, #wintype
            0, #fc
            samp_rate, #bw
            "", #name
            1
        )
        self.qtgui_freq_sink_x_0.set_update_time(0.10)
        self.qtgui_freq_sink_x_0.set_y_axis(-140, 10)
        self.qtgui_freq_sink_x_0.set_y_label('Relative Gain', 'dB')
        self.qtgui_freq_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qtgui_freq_sink_x_0.enable_autoscale(False)
        self.qtgui_freq_sink_x_0.enable_grid(True)
        self.qtgui_freq_sink_x_0.set_fft_average(0.05)
        self.qtgui_freq_sink_x_0.enable_axis_labels(True)
        self.qtgui_freq_sink_x_0.enable_control_panel(True)



        labels = ['', '', '', '', '',
            '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
            "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]

        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_freq_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_freq_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_freq_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_freq_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_freq_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_freq_sink_x_0_win = sip.wrapinstance(self.qtgui_freq_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_freq_sink_x_0_win)
        self.digital_chunks_to_symbols_xx_0 = digital.chunks_to_symbols_bc(xmod.points(), 1)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate,True)
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_gr_complex*1, len(subcarrier_map))
        self.analog_random_source_x_0 = blocks.vector_source_b(list(map(int, numpy.random.randint(0, len(xmod.points()), 123567))), True)


        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_random_source_x_0, 0), (self.digital_chunks_to_symbols_xx_0, 0))
        self.connect((self.blocks_stream_to_vector_0, 0), (self.synt_polyphase_rotator, 0))
        self.connect((self.blocks_throttle_0, 0), (self.qtgui_freq_sink_x_0, 0))
        self.connect((self.digital_chunks_to_symbols_xx_0, 0), (self.blocks_stream_to_vector_0, 0))
        self.connect((self.synt_fft, 0), (self.synt_polyphase_synt_filter, 0))
        self.connect((self.synt_periodic_multiply_const, 0), (self.blocks_throttle_0, 0))
        self.connect((self.synt_polyphase_rotator, 0), (self.synt_subcarrier_remapper, 0))
        self.connect((self.synt_polyphase_synt_filter, 0), (self.synt_periodic_multiply_const, 0))
        self.connect((self.synt_subcarrier_remapper, 0), (self.synt_fft, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "test_streaming_evm")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_num_channels(self):
        return self.num_channels

    def set_num_channels(self, num_channels):
        self.num_channels = num_channels
        self.set_fbmc_fft_len(int(self.num_channels * 5 // 4))
        self.set_fbmc_tx_stride({16: 22, 20: 28, 24: 34, 32: 46, 40: 58}[self.num_channels])

    def get_fbmc_tx_stride(self):
        return self.fbmc_tx_stride

    def set_fbmc_tx_stride(self, fbmc_tx_stride):
        self.fbmc_tx_stride = fbmc_tx_stride
        self.set_fbmc_eq_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "eq"))
        self.set_fbmc_rx_stride(self.fbmc_tx_stride)
        self.set_fbmc_rx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "rx"))
        self.set_fbmc_tx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "tx"))

    def get_fbmc_fft_len(self):
        return self.fbmc_fft_len

    def set_fbmc_fft_len(self, fbmc_fft_len):
        self.fbmc_fft_len = fbmc_fft_len
        self.set_fbmc_eq_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "eq"))
        self.set_fbmc_rx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "rx"))
        self.set_fbmc_tx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "tx"))
        self.set_subcarrier_inv([subcarrier_map.index(x) if x in self.subcarrier_map else -1 for x in range(self.fbmc_fft_len)])
        self.synt_periodic_multiply_const.set_vals(np.exp(1.0j * np.pi / self.fbmc_fft_len * np.arange(0, 2 * self.fbmc_fft_len)))

    def get_fbmc_eq_taps(self):
        return self.fbmc_eq_taps

    def set_fbmc_eq_taps(self, fbmc_eq_taps):
        self.fbmc_eq_taps = fbmc_eq_taps
        self.set_fbmc_eq2_taps([self.fbmc_eq_taps[i // 2] if i % 2 == 0 else 0.0 for i in range(2*len(self.fbmc_eq_taps)-1)])

    def get_subcarrier_map(self):
        return self.subcarrier_map

    def set_subcarrier_map(self, subcarrier_map):
        self.subcarrier_map = subcarrier_map
        self.set_subcarrier_inv([subcarrier_map.index(x) if x in self.subcarrier_map else -1 for x in range(self.fbmc_fft_len)])

    def get_fbmc_rx_taps(self):
        return self.fbmc_rx_taps

    def set_fbmc_rx_taps(self, fbmc_rx_taps):
        self.fbmc_rx_taps = fbmc_rx_taps
        self.set_fbmc_rxeq_taps(np.convolve(self.fbmc_rx_taps, self.fbmc_eq2_taps, mode="full"))

    def get_fbmc_eq2_taps(self):
        return self.fbmc_eq2_taps

    def set_fbmc_eq2_taps(self, fbmc_eq2_taps):
        self.fbmc_eq2_taps = fbmc_eq2_taps
        self.set_fbmc_rxeq_taps(np.convolve(self.fbmc_rx_taps, self.fbmc_eq2_taps, mode="full"))

    def get_xmod(self):
        return self.xmod

    def set_xmod(self, xmod):
        self.xmod = xmod

    def get_subcarrier_inv(self):
        return self.subcarrier_inv

    def set_subcarrier_inv(self, subcarrier_inv):
        self.subcarrier_inv = subcarrier_inv

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)
        self.qtgui_freq_sink_x_0.set_frequency_range(0, self.samp_rate)

    def get_fbmc_tx_taps(self):
        return self.fbmc_tx_taps

    def set_fbmc_tx_taps(self, fbmc_tx_taps):
        self.fbmc_tx_taps = fbmc_tx_taps

    def get_fbmc_rxeq_taps(self):
        return self.fbmc_rxeq_taps

    def set_fbmc_rxeq_taps(self, fbmc_rxeq_taps):
        self.fbmc_rxeq_taps = fbmc_rxeq_taps

    def get_fbmc_rx_stride(self):
        return self.fbmc_rx_stride

    def set_fbmc_rx_stride(self, fbmc_rx_stride):
        self.fbmc_rx_stride = fbmc_rx_stride




def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-n", "--num-channels", dest="num_channels", type=intx, default=16,
        help="Set number of channels (must be multiple of 4) [default=%(default)r]")
    return parser


def main(top_block_cls=test_streaming_evm, options=None):
    if options is None:
        options = argument_parser().parse_args()

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls(num_channels=options.num_channels)

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    def quitting():
        tb.stop()
        tb.wait()

    qapp.aboutToQuit.connect(quitting)
    qapp.exec_()

if __name__ == '__main__':
    main()
