#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Test Vector Filter Speed
# GNU Radio version: 3.8.2.0

from gnuradio import blocks
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import marmote3


class test_vector_filter_speed(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Test Vector Filter Speed")

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 80000000
        self.num_channels = num_channels = 16

        ##################################################
        # Blocks
        ##################################################
        self.marmote3_vector_fir_filter_xxx_0 = marmote3.vector_fir_filter_fff(num_channels, list(range(40)))
        self.blocks_vector_to_stream_0_0 = blocks.vector_to_stream(gr.sizeof_float*1, num_channels)
        self.blocks_vector_source_x_0_0 = blocks.vector_source_f(range(1024), True, 1, [])
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_float*1, samp_rate,True)
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_float*1, num_channels)
        self.blocks_probe_rate_0_0 = blocks.probe_rate(gr.sizeof_float*1, 500.0, 0.15)
        self.blocks_message_debug_0_0 = blocks.message_debug()



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_probe_rate_0_0, 'rate'), (self.blocks_message_debug_0_0, 'print'))
        self.connect((self.blocks_stream_to_vector_0, 0), (self.marmote3_vector_fir_filter_xxx_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.blocks_stream_to_vector_0, 0))
        self.connect((self.blocks_vector_source_x_0_0, 0), (self.blocks_throttle_0, 0))
        self.connect((self.blocks_vector_to_stream_0_0, 0), (self.blocks_probe_rate_0_0, 0))
        self.connect((self.marmote3_vector_fir_filter_xxx_0, 0), (self.blocks_vector_to_stream_0_0, 0))


    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)

    def get_num_channels(self):
        return self.num_channels

    def set_num_channels(self, num_channels):
        self.num_channels = num_channels





def main(top_block_cls=test_vector_filter_speed, options=None):
    tb = top_block_cls()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    try:
        input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
