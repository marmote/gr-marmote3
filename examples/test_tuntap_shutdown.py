#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Test Tuntap Shutdown
# GNU Radio version: 3.8.2.0

from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import marmote3


class test_tuntap_shutdown(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Test Tuntap Shutdown")

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 32000

        ##################################################
        # Blocks
        ##################################################
        self.marmote3_test_message_sink_0 = marmote3.test_message_sink(1)
        self.marmote3_test_message_sink_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_fixed_tuntap_pdu_0 = marmote3.fixed_tuntap_pdu('tap0', 1500, False)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.marmote3_fixed_tuntap_pdu_0, 'out'), (self.marmote3_test_message_sink_0, 'in'))


    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate





def main(top_block_cls=test_tuntap_shutdown, options=None):
    tb = top_block_cls()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    try:
        input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
