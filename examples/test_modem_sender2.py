#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Test Modem Sender2
# GNU Radio version: 3.8.2.0

from gnuradio import blocks
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import marmote3


class test_modem_sender2(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Test Modem Sender2")

        ##################################################
        # Variables
        ##################################################
        self.modem_monitor = marmote3.modem_monitor(1,
            "tcp://127.0.0.1:7557", 2, "tcp://127.0.0.1:7555")
        self.freq = freq = 1e6

        ##################################################
        # Blocks
        ##################################################
        _unused = self.msg_connect((self.modem_monitor, 'unused'),
            (self.modem_monitor, 'unused'))
        self.marmote3_test_message_source_0 = marmote3.test_message_source(1200, 1200, 4, 2000000, 200, 501, 101, 102)
        self.marmote3_test_message_source_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_test_message_sink_0 = marmote3.test_message_sink(4)
        self.marmote3_test_message_sink_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_modem_sender2_0 = marmote3.modem_sender2(1400, 2*1400, 2*1400, 20000, 1, 1.0, -1, True, 4, 102, [], 0, False, True)
        self.marmote3_modem_sender2_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_modem_receiver_0 = marmote3.modem_receiver(4, getattr(self, 'marmote3_modem_sender2_0') if 'marmote3_modem_sender2_0' else marmote3.modem_sender2_sptr(), False)
        self.marmote3_modem_receiver_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_modem_control_0 = marmote3.modem_control('freq', 'none', 'none', 'none', 'none', 'none', 'none')
        if 'freq' != 'none':
            self.marmote3_modem_control_0.set_center_freq(self.freq)
        if 'none' != 'none':
            self.marmote3_modem_control_0.set_samp_rate(self.none)
        if 'none' != 'none':
            self.marmote3_modem_control_0.set_rx_gain(self.none)
        if 'none' != 'none':
            self.marmote3_modem_control_0.set_tx_gain(self.none)
        if 'none' != 'none':
            self.marmote3_modem_control_0.set_tx_power(self.none)
        if 'none' != 'none':
            self.marmote3_modem_control_0.set_invert_spectrum(self.none)
        if 'none' != 'none':
            self.marmote3_modem_control_0.set_enable_recording(self.none)
        if 'none' != 'none':
            self.marmote3_modem_control_0.set_num_channels(self.none)
        if 'none' != 'none':
            self.marmote3_modem_control_0.set_num_subcarriers(self.none)
        self._modem_setter = marmote3.ModemSetter(self)
        self.marmote3_modem_control_0.set_modem_setter(self._modem_setter)
        self.marmote3_modem_control_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_header_decompressor_0 = marmote3.header_decompressor(4, False)
        self.marmote3_header_decompressor_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_header_compressor_0 = marmote3.header_compressor(4, False)
        self.marmote3_header_compressor_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_char*1, freq,True)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.marmote3_header_compressor_0, 'out'), (self.marmote3_modem_sender2_0, 'in'))
        self.msg_connect((self.marmote3_header_decompressor_0, 'out'), (self.marmote3_test_message_sink_0, 'in'))
        self.msg_connect((self.marmote3_modem_receiver_0, 'out'), (self.marmote3_header_decompressor_0, 'in'))
        self.msg_connect((self.marmote3_test_message_source_0, 'out'), (self.marmote3_header_compressor_0, 'in'))
        self.connect((self.blocks_throttle_0, 0), (self.marmote3_modem_receiver_0, 0))
        self.connect((self.marmote3_modem_sender2_0, 0), (self.blocks_throttle_0, 0))


    def get_variable_marmote3_modem_monitor_0(self):
        return self.variable_marmote3_modem_monitor_0

    def set_variable_marmote3_modem_monitor_0(self, variable_marmote3_modem_monitor_0):
        self.variable_marmote3_modem_monitor_0 = variable_marmote3_modem_monitor_0

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.blocks_throttle_0.set_sample_rate(self.freq)
        self.marmote3_modem_control_0.set_center_freq(self.freq)





def main(top_block_cls=test_modem_sender2, options=None):
    tb = top_block_cls()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    try:
        input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
